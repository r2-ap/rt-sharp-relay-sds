module config;

import vibe.d;
import std.string;

shared static class Config {
shared: static:
	bool EnableIPV4;
	bool EnableIPV6;
	ushort ServerPort;
	string AccessPassword;
	string SCGIHost;
	ushort SCGIPort;
	bool SSLEnabled;
	string SSLPrivateKey;
	string SSLPublicKey;
	string SSLCipherList;
	ushort SSLServerPort;
}
