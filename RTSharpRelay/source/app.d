import vibe.d;
import onyx.bundle;
import config;
import handle;
import sendEx;
import std.conv;
import std.algorithm;
import core.time;
import core.memory;

import etc.linux.memoryerror;

const string VERSION = "0.1.0";
shared immutable ulong I_VERSION;

TLSContext SSLCtx;

shared static this()
{
	import std.c.stdlib : exit;

	bool ver;
	readOption("version", &ver, "Prints version");
	if (ver) {
		logInfo("RT#-Relay server v%s\n\n", VERSION);
		exit(1);
		return;
	}

	static if (is(typeof(registerMemoryErrorHandler)))
		registerMemoryErrorHandler();

	auto vSplits = split(VERSION, '.');
	I_VERSION = (to!ulong(vSplits[0]) << 40) + (to!ulong(vSplits[1]) << 32) + to!uint(vSplits[2]);

	auto config = new immutable Bundle("config.conf");
	Config.EnableIPV4 = to!bool(config.value!int("Server", "EnableIPV4"));
	Config.EnableIPV6 = to!bool(config.value!int("Server", "EnableIPV6"));
	Config.ServerPort = config.value!ushort("Server", "ServerPort");
	Config.AccessPassword = config.value("Server", "AccessPassword");
	if (Config.AccessPassword == "CHANGE_ME") {
		logFatal("Please change access password.");
		exit(1);
	}
	if (Config.AccessPassword.length < 15) {
		logFatal("Please choose a longer access password (>=15 chars). It's for your own safety. Use a SECURE random password generator.");
		exit(1);
	}
	Config.SCGIHost = config.value("Remote", "SCGIHost");
	Config.SCGIPort = config.value!ushort("Remote", "SCGIPort");
	Config.SSLEnabled = to!bool(config.value!int("SSL", "Enabled"));
	if (Config.SSLEnabled) {
		Config.SSLPrivateKey = config.value("SSL", "PrivateKey");
		Config.SSLPublicKey = config.value("SSL", "PublicKey");
		Config.SSLCipherList = config.value("SSL", "CipherList");
		Config.SSLServerPort = config.value!ushort("SSL", "ServerPort");
	}
	auto closeOnInvalidPassword = to!bool(config.value!int("Server", "CloseOnInvalidPassowrd"));

	TCPListenOptions options =
		TCPListenOptions.distribute |
		(closeOnInvalidPassword ? cast(TCPListenOptions)0 : TCPListenOptions.disableAutoClose) |
		TCPListenOptions.reusePort;

	logInfo("Listening on:");

	if (Config.EnableIPV4) {
		listenTCP(Config.ServerPort, (conn) => TCPHandle(conn, conn), "0.0.0.0", options);
		logInfo("0.0.0.0");
		if (Config.SSLEnabled) {
			listenTCP(Config.SSLServerPort, (conn) => TCPHandleSSL(conn), "0.0.0.0", options);
			logInfo("0.0.0.0 SSL (%s, %s)", Config.SSLPublicKey, Config.SSLPrivateKey);
		}
	}
	if (Config.EnableIPV6) {
		logInfo("::");
		listenTCP(Config.ServerPort, (conn) => TCPHandle(conn, conn), "::", options);
		if (Config.SSLEnabled) {
			listenTCP(Config.SSLServerPort, (conn) => TCPHandleSSL(conn), "::", options);
			logInfo(":: SSL (%s, %s)", Config.SSLPublicKey, Config.SSLPrivateKey);
		}
	}

	if (Config.SSLEnabled) {
		SSLCtx = createTLSContext(TLSContextKind.server, TLSVersion.tls1_2);
		SSLCtx.useCertificateChainFile(Config.SSLPublicKey);
		SSLCtx.usePrivateKeyFile(Config.SSLPrivateKey);
		SSLCtx.setCipherList(Config.SSLCipherList);
		logDebug("SSL Set");
	}

	logDebug("Setting actions...");
	auto conn = connectTCP(Config.SCGIHost, Config.SCGIPort);
	if (!conn.connected) {
		logDiagnostic("Failed to connect to rtorrent. Did you open SCGI on %d?", Config.SCGIPort);
		return;
	}

	auto xml = GetRTXmlResponse(conn, "<?xml version=\"1.0\" ?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data><value><struct><member><name>methodName</name><value><string>system.method.set_key</string></value></member><member><name>params</name><value><array><data><value><string>event.download.finished</string></value>
<value><string>seedingtimertsrelay</string></value><value><string>d.set_custom=seedingtime,\"$execute_capture={date,+%s}\"</string></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>system.method.set_key</string></value></member><member><name>params</name><value><array><data><value><string>event.download.inserted_new</string></value><value><string>addtimertsrelay</string></value><value><string>d.set_custom=addtime,\"$execute_capture={date,+%s}\"</string></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>system.method.set_key</string></value></member><member><name>params</name><value><array><data><value><string>event.download.hash_done</string></value><value><string>seedingtimecheckrtsrelay</string></value><value><string>branch=$not=$d.get_complete=,,d.get_custom=seedingtime,,\"d.set_custom=seedingtime,$d.get_custom=addtime\"</string></value></data></array></value></member></struct></value></data></array></value></param></params></methodCall>");
	logDebugV("Actions return: %s", xml.toString());
	logDebug("Actions set");
}

void TCPHandleSSL(TCPConnection conn) {
	Stream stm = conn;

	stm = createTLSStream(stm, SSLCtx, TLSStreamState.accepting);
	logTrace("Created stream SSL");
	runTask(() => TCPHandle(stm, conn));
	logTrace("Handled SSL");
}

void TCPHandle(Stream stm, TCPConnection tcp)
{
	Client client;

	ubyte[] data;
	data.length = Config.AccessPassword.length;
	stm.read(data);

	if (data != Config.AccessPassword) {
		logDiagnostic("Invalid password from %s (got %s)", tcp.remoteAddress.toString(), data[0..$]);
		return; // Leave it hanging (if config option is not set), if we close connection immediately after wrong password, user will know password length
	}

	client.Stm = stm;

	HandleClient(client);
}
