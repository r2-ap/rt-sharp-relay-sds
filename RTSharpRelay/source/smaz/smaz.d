module smaz;

import std.array;
import std.algorithm;
import std.stdio;

class Smaz
{
	immutable string[] _termsTable = [
		".", "The", "e", "t", "a", "of", "o", ".720p.", "i", "n", "s", "e ", "r", " th",
		" t", "in", "he", "th", "h", "he ", "to", "] [", "l", "s ", "d", ".B", "an",
		"er", "c", ".mkv", "d ", "on", " of", "re", "of ", "t ", ", ", "Is", "u", "At",
		" [V0]", "n ", "Or", " / ", "f", "m", "as", "It", "That", "WEB", "Was", "en",
		" [V2]", "VA", "es", "96/24", "TV", "f ", "g", "p", "nd", " (", "nd ", "ed ",
		"w", "ed", "MP3", "FLAC", "for", "te", "ing", "y ", "The", ".exe", "ti", "r ", "his",
		"st", " [", "ar", "nt", ",", " to", "y", "ng", " h", "with", "le", "al", "to ",
		"b", "ou", "be", "Remux", ".On.", "se", " (", "ent", "ha", "ng", ".DTS.", "-",
		"hi", "Web", "BDRip", "In ", "de", "ion", "me", "v", ".mp3", "ve", "all", "re ",
		"ri", "ro", "Is ", "co", ".epub", "are", "ea", "o ", "her", " m", "er ", " p",
		"es ", "by", "320", "di", "ra", "ic", "not", "24", "Remux", "at ", "ce", "la",
		"h ", "ne", "as ", "tio", "200", "201", "io", "we", "199", "om", "2016", "BluRay",
		"ur", "li", "ll", "ch", "Rip", ".1080p.", "WEBRip", "2015", "DD5", "ere",
		" S", "DVDRip", "a ", "us", " M", "ss", " be", "1.",
		"(201", "ma", "V0", "REPACK", "or ", "But", "el", "so", "l ", "And", "s,", "no",
		"ter", ".S0", "iv", "ho", "HD", " B", "hat", "08", "ns", "ch ", "wh", "tr",
		"ut", ") [", "Have", "ly ", "ta", ".XXX.", " on", "tha", " & ", " [320]", "ati", "en ",
		"pe", " re", "XviD", "V2", "si", "E0", "wa", "CD", "Our", "who", "Its", "z",
		"fo", "rs", "ot", "un", "im", "th ", "nc", "ate", "HDTV", "ad",
		" we", "ly", "MA", " n", "id", " cl", "ac", "il", "rt", " wi",
		" - ", " it", "whi", " ma", "ge", "x", "e c", "Edition", ".x264-"
	];

	ubyte[][] _termsTableBytes;
	ubyte[][] _hashTable;
	int _maxTermSize;
	int _maxVerbatimLen;

	this()
	{
		if (_termsTable.length + 8 > ubyte.max)
			throw new Exception("Too many terms defined");

		_termsTableBytes = uninitializedArray!(ubyte[][])(_termsTable.length);
		_maxVerbatimLen = cast(int)(ubyte.max - _termsTable.length);
		_hashTable = uninitializedArray!(ubyte[][])(ubyte.max);

		for (int i = 0; i < _termsTable.length; i++) {
			auto bytes = cast(ubyte[])_termsTable[i].dup;
			if (bytes.length > ubyte.max)
				throw new Exception("Term " ~ _termsTable[i] ~ " is too big");
			_termsTableBytes[i] = bytes;
			ubyte[] buffer = uninitializedArray!(ubyte[])(bytes.length + 2);
			buffer[0] = cast(ubyte)bytes.length;
			buffer[buffer.length - 1] = cast(ubyte)i;

			buffer[1..1 + bytes.length] = bytes[0..bytes.length];
			_maxTermSize = cast(int)(max(_maxTermSize, bytes.length));

			int h = bytes[0] << 3;
			AddToHash(h, buffer);
			if (bytes.length == 1)
				continue;
			h += bytes[1];
			AddToHash(h, buffer);
			if (bytes.length == 2)
				continue;
			h ^= bytes[2];
			AddToHash(h, buffer);
		}
	}

	void AddToHash(int hash, ubyte[] buffer)
	{
		auto index = hash % _hashTable.length;
		if (_hashTable[index] == null) {
			_hashTable[index] = buffer;
			return;
		}

		ubyte[] newBuffer = uninitializedArray!(ubyte[])(_hashTable[index].length + buffer.length);
		newBuffer[0.._hashTable[index].length] = _hashTable[index][0..$];
		newBuffer[_hashTable[index].length.._hashTable[index].length + buffer.length] = buffer[0..$];
		_hashTable[index] = newBuffer;
	}

	public string Decompress(string Input) {
		auto bytes = cast(ubyte[])Input;
		ubyte[] outp = uninitializedArray!(ubyte[])(bytes.length * _maxTermSize);
		auto size = Decompress(bytes, cast(int)bytes.length, outp);
		return cast(string)(outp[0..size]);
	}

	public int Decompress(ubyte[] input, int inputLen, ubyte[] output)
	{
		auto outPos = 0;
		for (int i = 0; i < inputLen; i++) {
			auto slot = input[i];
			if (slot >= _termsTable.length) {
				auto len = slot - _termsTable.length;
				output[outPos..outPos + len] = input[i + 1..i + 1 + len];
				outPos += len;
				i += len;
			} else {
				output[outPos..outPos + _termsTableBytes[slot].length] = _termsTableBytes[slot][0..$];
				outPos += _termsTableBytes[slot].length;
			}
		}
		return outPos;
	}

	public string Compress(string Input) {
		auto bytes = cast(ubyte[])Input;
		ubyte[] outp = uninitializedArray!(ubyte[])(bytes.length * 4);
		auto size = Compress(bytes, outp);
		return cast(string)(outp[0..size]);
	}

	public int Compress(ubyte[] input, ubyte[] output)
	{
		auto outPos = 0;
		auto verbatimStart = 0;
		auto verbatimLength = 0;
		for (int i = 0; i < input.length; i++) {
			auto size = _maxTermSize;
			int h1, h2 = 0, h3 = 0;
			h1 = input[i] << 3;
			if (i + 1 < input.length)
				h2 = h1 + input[i + 1];
			if (i + 2 < input.length)
				h3 = h2 ^ input[i + 2];
			if (i + size >= input.length)
				size = cast(int)input.length - i;
			// whoosh
			auto foundMatch = false;
			for (; size > 0 && !foundMatch; size--) {
				ubyte[] slot;
				switch (size) {
					case 1:	slot = _hashTable[h1 % _hashTable.length]; break;
					case 2:	slot = _hashTable[h2 % _hashTable.length]; break;
					default:slot = _hashTable[h3 % _hashTable.length]; break;
				}
				int pos = 0;
				while (pos + 1 < slot.length) {
					auto termLength = slot[pos];
					if (termLength != size || !BufferEquals(slot, pos + 1, input, i, size)) {
						pos += termLength + 2;
						continue;
					}
					if (verbatimLength > 0) {
						Flush(input, output, verbatimStart, verbatimLength, outPos);
					}
					output[outPos++] = slot[termLength + pos + 1];
					verbatimStart = i + termLength;
					i += termLength - 1;
					foundMatch = true;
					break;
				}
			}
			if (!foundMatch)
				verbatimLength++;
		}
		Flush(input, output, verbatimStart, verbatimLength, outPos);
		return outPos;
	}

	void Flush(ubyte[] input, ubyte[] output, ref int verbatimStart, ref int verbatimLength, ref int outPos) {
		while (verbatimLength > 0) {
			auto len = min(_maxVerbatimLen - 1, verbatimLength);
			output[outPos++] = cast(ubyte)(len + _termsTable.length);
			output[outPos..outPos + len] = input[verbatimStart..verbatimStart + len];
			verbatimStart += len;
			verbatimLength -= len;
			outPos += len;
		}
	}

	bool BufferEquals(ubyte[] x, int xStart, ubyte[] y, int yStart, int count)
	{
		for (int i = 0; i < count; i++) {
			if (y[yStart + i] != x[xStart + i])
				return false;
		}
		return true;
	}
}

__gshared Smaz cSmaz = new Smaz();

debug unittest
{
	auto smaz = new Smaz();
	writeln("Unit tests...");
	writeln("Compressing \"Testing Test - Exclusive Test Tracks - Deluxe Edition - (2016) [FLAC]\"...");
	assert(smaz.Compress("Testing Test - Exclusive Test Tracks - Deluxe Edition - (2016) [FLAC]") ==
		   "\xF6\x54\x36\x4A\x54\xF7\x20\x54\x36\x23\x65\xF7\x20\x45\xF1\x1C\x16\xA3\xB6\x0B\xF6\x54\x36\x23\xF6\x54\x82\x1C\xF6\x6B\x17\x65\xF7\x20\x44\xAE\x26\xF1\x0B\xF3\xEC\xA8\xF6\x36\xC1\x43\xF6\x5D");
	writeln("Decompressing \"F654364A54F72054362365F72045F11C16A3B60BF6543623F654821CF66B1765F72044AE26F10BF3ECA8F636C143F65D\"...");
	assert(smaz.Decompress("\xF6\x54\x36\x4A\x54\xF7\x20\x54\x36\x23\x65\xF7\x20\x45\xF1\x1C\x16\xA3\xB6\x0B\xF6\x54\x36\x23\xF6\x54\x82\x1C\xF6\x6B\x17\x65\xF7\x20\x44\xAE\x26\xF1\x0B\xF3\xEC\xA8\xF6\x36\xC1\x43\xF6\x5D") ==
		   "Testing Test - Exclusive Test Tracks - Deluxe Edition - (2016) [FLAC]");

	writeln("Compressing \"Ｕｎｉｃｏｄｅ.Ｂｌｏｃｋｂｕｓｔｅｒ.Cant.Get.Enough.Tests.x264-TESTING\"...");
	assert(smaz.Compress("Ｕｎｉｃｏｄｅ.Ｂｌｏｃｋｂｕｓｔｅｒ.Cant.Get.Enough.Tests.x264-TESTING") ==
		   "\xFE\xEF\xBC\xB5\xEF\xBD\x8E\xEF\xBD\x89\xFE\xEF\xBD\x83\xEF\xBD\x8F\xEF\xBD\x84\xF8\xEF\xBD\x85\x00\xFE\xEF\xBC\xA2\xEF\xBD\x8C\xEF\xBD\x8F\xFE\xEF\xBD\x83\xEF\xBD\x8B\xEF\xBD\x82\xFE\xEF\xBD\x95\xEF\xBD\x93\xEF\xBD\x94\xFB\xEF\xBD\x85\xEF\xBD\x92\x00\xF6\x43\x1A\x03\x00\xF6\x47\x02\x03\x00\xF6\x45\xB3\x26\x3A\x12\x00\xF6\x54\x36\x03\x0A\xF4\xFC\x54\x45\x53\x54\x49\x4E\x47");
	writeln("Decompressing \"FEEFBCB5EFBD8EEFBD89FEEFBD83EFBD8FEFBD84F8EFBD8500FEEFBCA2EFBD8CEFBD8FFEEFBD83EFBD8BEFBD82FEEFBD95EFBD93EFBD94FBEFBD85EFBD9200F6431A0300F647020300F645B3263A1200F65436030AF4FC54455354494E47\"...");
	assert(smaz.Decompress("\xFE\xEF\xBC\xB5\xEF\xBD\x8E\xEF\xBD\x89\xFE\xEF\xBD\x83\xEF\xBD\x8F\xEF\xBD\x84\xF8\xEF\xBD\x85\x00\xFE\xEF\xBC\xA2\xEF\xBD\x8C\xEF\xBD\x8F\xFE\xEF\xBD\x83\xEF\xBD\x8B\xEF\xBD\x82\xFE\xEF\xBD\x95\xEF\xBD\x93\xEF\xBD\x94\xFB\xEF\xBD\x85\xEF\xBD\x92\x00\xF6\x43\x1A\x03\x00\xF6\x47\x02\x03\x00\xF6\x45\xB3\x26\x3A\x12\x00\xF6\x54\x36\x03\x0A\xF4\xFC\x54\x45\x53\x54\x49\x4E\x47") ==
			"Ｕｎｉｃｏｄｅ.Ｂｌｏｃｋｂｕｓｔｅｒ.Cant.Get.Enough.Tests.x264-TESTING");
	writeln("Unit tests done");
}