module actions;

import std.variant;
import std.algorithm;
import std.algorithm.iteration;
import std.file;
import std.socket;
import vibe.d;

import app;
import sendEx;
import xmls;
import config;
import xpaths;
import kxml.xml;
import std.xml;
import smaz;

void ActVersion(ref Client client)
{
	ubyte[] toWrite;

	debug logDebug("Version: %d", I_VERSION);

	WriteIntToBuffer(toWrite, I_VERSION);
	client.Stm.write(toWrite);
}

// mixin
string XML(int In)
{
	return `fields[(x * totalFields)+` ~ to!string(In) ~ `].getInnerXML()`;
}

void ActGetTorrents(ref Client client, Variant[] Data, XmlNode ResXml)
{
    import core.stdc.string;

	auto fields = ResXml.parseXPath(XPaths.ALL_TORRENTS);

	TORRENT curTorrent;
	bool state, rechecking, priv;
    ubyte[] toWrite, writeTracker;

	auto sendOnlyUpdate = Data[0].get!(bool);
	auto totalFields = Data[1].get!(int);
	auto stm = client.Stm;

	if (!sendOnlyUpdate)
		client.LastTorrentList.length = 0;

	TORRENT[] sendList;
	TORRENT[] curTorrentList;
    ushort[string] trackerTable;
    ushort tableInc = 0;

    for (auto x = 0;x < fields.length / totalFields;x++) {
        auto trackers = split(mixin(XML(12)), "*");

        foreach (trk; trackers) {
            if (trk == "")
                continue;

            string sUri = decode(trk[0..indexOf(trk, '@')]);

            if (sUri !in trackerTable) {
                trackerTable[sUri] = tableInc;

                tableInc++;
                WriteStringToBuffer(toWrite, sUri.dup);
            }
        }
    }
    trackerTable.rehash();

    WriteIntToBuffer(writeTracker, tableInc);
    stm.write(writeTracker);
    stm.write(toWrite);
    toWrite.length = 0;

	for (auto x = 0;x < fields.length / totalFields;x++) {
		curTorrent.Name = cSmaz.Compress(decode(mixin(XML(0))));
		curTorrent.Size = to!ulong(mixin(XML(1)));
		curTorrent.Downloaded = to!ulong(mixin(XML(2)));
		curTorrent.Uploaded = to!ulong(mixin(XML(3)));
		curTorrent.DLSpeed = to!uint(mixin(XML(4)));
		curTorrent.UPSpeed = to!uint(mixin(XML(5)));
		curTorrent.CreatedOn = mixin(XML(6)) == "" ? 0 : to!ulong(mixin(XML(6)));
		curTorrent.AddedOn = mixin(XML(7)) == "" ? 0 : to!ulong(mixin(XML(7)));
		curTorrent.FinishedOn = mixin(XML(8)) == "" ? 0 : to!ulong(mixin(XML(8)));
		curTorrent.Label = decode(mixin(XML(9)).dup);
		curTorrent.Priority = cast(TORRENT_PRIORITY)(to!ubyte(mixin(XML(10))));
		curTorrent.ChunkSize = to!uint(mixin(XML(11)));

		auto trackers = split(mixin(XML(12)), "*");
		curTorrent.Trackers.length = 0;

		foreach (tr; trackers) {
			if (tr == "")
				continue;

			auto trk = new TRACKER;
			curTorrent.Trackers ~= trk;

			auto parts = split(tr, "@");
			trk.UriCode = trackerTable[decode(parts[0])];
			trk.Enabled = parts[1] == "1";
			trk.Seeders = to!uint(parts[2]);
			trk.Peers = to!uint(parts[3]);
			trk.Downloaded = to!uint(parts[4]);
			trk.LastUpdated = to!ulong(parts[5]);
			trk.Interval = to!uint(parts[6]);
		}

		curTorrent.StatusMsg = mixin(XML(13)).dup;

		// If not 'open'
		if (!(mixin(XML(14)) == "1")) {
			curTorrent.State = curTorrent.Size == curTorrent.Downloaded ? TORRENT_STATE.COMPLETE : TORRENT_STATE.STOPPED;
		} else {
			// 'state'
			if (mixin(XML(15)) == "1") {
				// 'rechecking'
				if (!(mixin(XML(16)) == "1"))
					curTorrent.State = (curTorrent.Size == curTorrent.Downloaded) ? TORRENT_STATE.SEEDING : TORRENT_STATE.DOWNLOADING;
				else
					curTorrent.State = TORRENT_STATE.HASHING;
			} else
				curTorrent.State = TORRENT_STATE.PAUSED;
		}

		// 'private'
		if ((mixin(XML(17)) == "1"))
			curTorrent.State |= TORRENT_STATE.PRIVATE;

		curTorrent.SeedersConnected = to!uint(mixin(XML(18)));
		curTorrent.SeedersTotal = to!uint(sum(map!(el => el == "" ? 0 : to!uint(el))(split(mixin(XML(19)), "+"))));
		curTorrent.PeersConnected = to!uint(mixin(XML(20)));
		curTorrent.PeersTotal = to!uint(sum(map!(el => el == "" ? 0 : to!uint(el))(split(mixin(XML(21)), "+"))));
		for (auto a = 0; a < (mixin(XML(22)).length / 2); a++)
			curTorrent.Hash[a] = mixin(XML(22))[2*a..2*(a+1)].to!ubyte(16);
		
		bool dontSend = true;

		if (!sendOnlyUpdate) {
			sendList ~= curTorrent;
			dontSend = false;
		} else {
			bool foundTorrent = false;
			for (auto i = 0;i < client.LastTorrentList.length;i++) {
				auto t = &(client.LastTorrentList[i]);
				if ((*t).Hash[] == curTorrent.Hash[]) {
					if ((*t).UPSpeed != curTorrent.UPSpeed ||
						(*t).PeersConnected != curTorrent.PeersConnected ||
						(*t).PeersTotal != curTorrent.PeersTotal ||
						(*t).SeedersConnected != curTorrent.SeedersConnected ||
						(*t).SeedersTotal != curTorrent.SeedersTotal ||
						(*t).DLSpeed != curTorrent.DLSpeed ||
						(*t).State != curTorrent.State ||
						(*t).Downloaded != curTorrent.Downloaded ||
						(*t).Uploaded != curTorrent.Uploaded ||
						(*t).Label != curTorrent.Label ||
						(*t).Priority != curTorrent.Priority ||
						(*t).StatusMsg != curTorrent.StatusMsg) {
						sendList ~= curTorrent;
						(*t) = curTorrent;
					}
					client.LastTorrentList.remove(i);
					client.LastTorrentList.length--; // ?????????
					i--;
					foundTorrent = true;
					break;
				}
			}
			if (!foundTorrent)
				sendList ~= curTorrent;
		}
		curTorrentList ~= curTorrent;
	}

	ubyte[20][] removeList;

	if (sendOnlyUpdate) {
		foreach (t;client.LastTorrentList) {
			removeList ~= t.Hash;
		}
	}

	client.LastTorrentList.length = curTorrentList.length;
	if (curTorrentList.length != 0)
		client.LastTorrentList[] = curTorrentList[];

	WriteIntToBuffer(toWrite, cast(uint)sendList.length);
	stm.write(toWrite);
	toWrite.length = 0;

	logInfo("sendList %d", sendList.length);

	WriteIntToBuffer(toWrite, cast(uint)removeList.length);
	stm.write(toWrite);
	toWrite.length = 0;

	logInfo("removeList %d", removeList.length);

	foreach (t;sendList) {
		WriteIntToBuffer(toWrite, cast(uint)(t.Name.length));
		toWrite ~= cast(ubyte[])t.Name;
		WriteIntToBuffer(toWrite, cast(ushort)t.State);
		WriteIntToBuffer(toWrite, t.Size);
		WriteIntToBuffer(toWrite, t.Downloaded);
		WriteIntToBuffer(toWrite, t.Uploaded);
		WriteIntToBuffer(toWrite, t.DLSpeed);
		WriteIntToBuffer(toWrite, t.UPSpeed);
		WriteIntToBuffer(toWrite, t.CreatedOn);
		WriteIntToBuffer(toWrite, t.AddedOn);
		WriteIntToBuffer(toWrite, t.FinishedOn);
		WriteStringToBuffer(toWrite, t.Label);
		WriteIntToBuffer(toWrite, t.SeedersConnected);
		WriteIntToBuffer(toWrite, t.SeedersTotal);
		WriteIntToBuffer(toWrite, t.PeersConnected);
		WriteIntToBuffer(toWrite, t.PeersTotal);
		WriteIntToBuffer(toWrite, cast(byte)t.Priority);
		WriteIntToBuffer(toWrite, t.ChunkSize);

         WriteIntToBuffer(toWrite, cast(ushort)t.Trackers.length);
        foreach (trk; t.Trackers) {
            WriteIntToBuffer(toWrite, trk.UriCode);
            WriteIntToBuffer(toWrite, trk.Enabled);
            WriteIntToBuffer(toWrite, trk.Seeders);
            WriteIntToBuffer(toWrite, trk.Peers);
            WriteIntToBuffer(toWrite, trk.Downloaded);
            WriteIntToBuffer(toWrite, trk.LastUpdated);
            WriteIntToBuffer(toWrite, trk.Interval);
        }

		WriteStringToBuffer(toWrite, t.StatusMsg);

		toWrite ~= t.Hash;
		stm.write(toWrite);
		toWrite.length = 0;
	}

	foreach (h;removeList)
		stm.write(h);
	//logInfo("OUT elapsed %d ms", sw.peek().msecs);
}

void ActLoadRaw(ref Client client, XmlNode ResXml)
{
	logInfo("LoadRaw: %s", ResXml.toString());
	// TODO: report of errors
}

void ActStartTorrentsMulti(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActStopTorrentsMulti(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActPauseTorrentsMulti(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActForceRecheckTorrentsMulti(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActReannounceTorrentsMulti(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActAddPeerTorrent(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActSetLabelTorrentsMulti(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActSetPriorityTorrentsMulti(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActRemoveTorrentsMulti(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActRemoveTorrentsPlusDataMulti(ref Client client, XmlNode ResXml)
{
	import std.file : DirEntry, rmdirRecurse;

	auto fields = ResXml.parseXPath(XPaths.RT_SETTINGS ~ "/string");

	foreach (XmlNode field; fields) {
		debug logWarn("rm -rf %s", decode(field.getInnerXML()));

		DirEntry obj;
		try	obj = DirEntry(decode(field.getInnerXML()));
		catch (Exception ex) {
			logWarn("RemoveTorrentsPlusDataMulti DirEntry failed: " ~ ex.msg);
			return;
		}

		if (obj.isDir)
			rmdirRecurse(obj);
		else
			remove(obj.name);
	}

	

	// TODO: report of errors
}

void ActGetTorrentFilesMulti(ref Client client, Variant[] Data)
{
	auto stm = client.Stm;
	ubyte[] toWrite;

	auto paths = Data[0].get!(string[]);

	WriteIntToBuffer(toWrite, cast(uint)paths.length);
	stm.write(toWrite);
	toWrite.length = 0;

	foreach (path; paths) {
		ubyte[] file;

		try {
			file = cast(ubyte[])read(path);
		} catch {
			// TODO: report of errors
		}

		WriteIntToBuffer(toWrite, cast(uint)file.length);
		stm.write(toWrite);
		stm.write(file);
		toWrite.length = 0;
	}
}

void ActGetTorrentPeers(ref Client client, Variant[] Data, XmlNode ResXml)
{
	auto fields = ResXml.parseXPath(XPaths.ALL_TORRENTS); // Same thing

	PEER curPeer;
	PEER[] sendList;
	ubyte[] toWrite;
	auto stm = client.Stm;
	auto totalFields = Data[0].get!(int);
	string curIp;

	if (fields.length == 0)
		goto noData;

	for (auto x = 0;x < fields.length / totalFields;x++) {
		for (auto a = 0; a < (mixin(XML(0)).length / 2); a++)
			curPeer.ID[a] = mixin(XML(0))[2*a..2*(a+1)].to!ubyte(16);
		curPeer.Client = decode(mixin(XML(1)).dup);
		curPeer.Downloaded = to!ulong(mixin(XML(2)));
		curPeer.Uploaded = to!ulong(mixin(XML(3)));
		curPeer.DLSpeed = to!uint(mixin(XML(4)));
		curPeer.UPSpeed = to!uint(mixin(XML(5)));
		curPeer.Flags |= (mixin(XML(6)) == "1" ? PEER_FLAGS.INCOMING : 0);
		curPeer.Flags |= (mixin(XML(7)) == "1" ? PEER_FLAGS.ENCRYPTED : 0);
		curPeer.Flags |= (mixin(XML(8)) == "1" ? PEER_FLAGS.SNUBBED : 0);
		curPeer.Flags |= (mixin(XML(9)) == "1" ? PEER_FLAGS.OBFUSCATED : 0);
		curIp = mixin(XML(10)).dup;
		
		if (curIp.indexOf(":") == -1)
			curPeer.Addr = new InternetAddress(curIp, to!ushort(mixin(XML(11))));
		else
			curPeer.Addr = new Internet6Address(curIp, to!ushort(mixin(XML(11))));

		sendList ~= curPeer;
	}

	if (sendList.length == 0) {
		goto noData;
	} else {
		WriteIntToBuffer(toWrite, cast(uint)sendList.length);
		stm.write(toWrite);
		toWrite.length = 0;
	}

	foreach (p; sendList) {
		toWrite ~= p.ID;
		if (p.Addr.type == typeid(Internet6Address)) {
			WriteIntToBuffer(toWrite, cast(ubyte)6);
			Internet6Address ipv6 = p.Addr.get!(Internet6Address);
			toWrite ~= ipv6.addr();
			WriteIntToBuffer(toWrite, cast(ushort)ipv6.port());
		} else {
			WriteIntToBuffer(toWrite, cast(ubyte)4);
			InternetAddress ipv4 = p.Addr.get!(InternetAddress);
			uint addr = ipv4.addr();
			WriteBinToBuffer(toWrite, addr);
			WriteIntToBuffer(toWrite, cast(ushort)ipv4.port());
		}
		WriteStringToBuffer(toWrite, p.Client);
		WriteIntToBuffer(toWrite, cast(ubyte)p.Flags);
		WriteIntToBuffer(toWrite, p.Downloaded);
		WriteIntToBuffer(toWrite, p.Uploaded);
		WriteIntToBuffer(toWrite, p.DLSpeed);
		WriteIntToBuffer(toWrite, p.UPSpeed);

		stm.write(toWrite);
		toWrite.length = 0;
	}

	return;
noData:
	logInfo("no data.");
    WriteIntToBuffer(toWrite, cast(uint)0);
	stm.write(toWrite);
}

void ActGetTorrentFiles(ref Client client, Variant[] Data, XmlNode ResXml)
{
	auto fields = ResXml.parseXPath(XPaths.ALL_TORRENTS); // Same thing

	FILE curFile;
	FILE[] sendList;
	ubyte[] toWrite;
	auto stm = client.Stm;
	auto totalFields = Data[0].get!(int);

	curFile.DownloadStrategy = FILE_DOWNLOAD_STRATEGY.NORMAL;

	for (auto x = 0;x < fields.length / totalFields;x++) {
		curFile.Size = to!ulong(mixin(XML(0)));
		curFile.Priority = cast(FILE_PRIORITY)(to!ubyte(mixin(XML(1))));
		curFile.Path = cSmaz.Compress(decode(mixin(XML(2))));
		
		curFile.DownloadedChunks = to!ulong(mixin(XML(3)));
		
		if (mixin(XML(4)) == "1")
			curFile.DownloadStrategy = FILE_DOWNLOAD_STRATEGY.LEADING_CHUNK_FIRST;
		if (mixin(XML(5)) == "1")
			curFile.DownloadStrategy = FILE_DOWNLOAD_STRATEGY.TRAILING_CHUCK_FIRST;

		sendList ~= curFile;
	}

	if (sendList.length == 0) {
		goto noData;
	} else {
		WriteIntToBuffer(toWrite, cast(uint)sendList.length);
		stm.write(toWrite);
		toWrite.length = 0;
	}

	foreach (f; sendList) {
		WriteStringToBuffer(toWrite, f.Path);
		WriteIntToBuffer(toWrite, f.Size);
		WriteIntToBuffer(toWrite, f.DownloadedChunks);
		WriteIntToBuffer(toWrite, cast(ubyte)f.Priority);
		WriteIntToBuffer(toWrite, cast(ubyte)f.DownloadStrategy);

		stm.write(toWrite);
		toWrite.length = 0;
	}

	return;
noData:
	logInfo("no data.");
	WriteIntToBuffer(toWrite, cast(uint)0);
    stm.write(toWrite);
}

void ActExecCustomCommand(ref Client client, XmlNode ResXml)
{
	auto stm = client.Stm;
    ubyte[] toWrite;

	if (ResXml is null) {
		logInfo("no data.");
        WriteIntToBuffer(toWrite, cast(uint)0);
		stm.write(toWrite); // HACK HACK
		return;
	}
	auto xml = ResXml.toString();
	logInfo("XML len %d", xml.length);

	WriteStringToBuffer(toWrite, xml);
	logInfo("XML %s", xml);
	stm.write(toWrite);
}

void ActGetSettings(ref Client client, XmlNode ResXml)
{
	auto stm = client.Stm;
	auto boolSettings = [ 6, 8, 10, 19, 31, 34, 36, 37, 40 ];

	auto fields = ResXml.parseXPath(XPaths.RT_SETTINGS);
	ubyte[] toWrite;

	for (auto x = 0;x < fields.length;x++) {
		auto s = fields[x].getInnerXML();
		
		if (startsWith(s, "<i8>")) {
			auto dataType = 11; // Data type long
			
			if (boolSettings.canFind(x))
				dataType = 3; // Data type bool

			toWrite ~= [ cast(ubyte)dataType ];
			
			auto data = s[4..$][0..$-5];
			if (dataType == 11)
				WriteIntToBuffer(toWrite, to!long(data));
			else
				toWrite ~= [ to!ubyte(data) ];
		}
		if (startsWith(s, "<string>")) {
			toWrite ~= [ cast(ubyte)18 ]; // Data type string
			
			auto data = s[8..$][0..$-9];
			WriteStringToBuffer(toWrite, decode(data));
		}
		if (s == "<string />") {
			toWrite ~= [ cast(ubyte)18 ];
            WriteStringToBuffer(toWrite, "");
        }
	}
	
	stm.write(toWrite);
}

void ActSetSettings(ref Client client, XmlNode ResXml)
{
	client.Stm.write([ cast(ubyte)1 ]); // All good :-DDD
}

void ActTransferFile(ref Client client)
{
	import std.path;
	import std.file;

    auto stm = client.Stm;
    ubyte[] toWrite;
    auto len = ReadInt!ulong(stm);
    auto dest = ReadString(stm);

	if (exists(dest)) {
		WriteIntToBuffer(toWrite, cast(ubyte)0);
        WriteStringToBuffer(toWrite, "File already exists");
		stm.write(toWrite);
		return;
	}

    FileStream fs;
	
	auto dir = dirName(dest);

	if (!exists(dir))
		createDirectory(dir);
	
    try fs = openFile(dest, FileMode.createTrunc);
    catch (Exception ex) {
        WriteIntToBuffer(toWrite, cast(ubyte)0);
        WriteStringToBuffer(toWrite, ex.msg);
		stm.write(toWrite);
        return;
    }

    WriteIntToBuffer(toWrite, cast(ubyte)1);

    stm.write(toWrite);

    try fs.write(stm, len);
    catch (Exception ex) {
        logWarn("Exception at ActTransferFile while writing file: %s", ex.msg);
    }
}

void ActToggleTracker(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActKickPeer(ref Client client, XmlNode ResXml)
{
	logInfo("KICK: %s", ResXml.toString());
	// TODO: report of errors
}

void ActBanPeer(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActToggleSnubPeer(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActSetFilePriority(ref Client client, XmlNode ResXml)
{
	logInfo(ResXml.toString());
	// TODO: report of errors
}

void ActSetFileDownloadStrategy(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActFileMediaInfo(ref Client client, XmlNode ResXml)
{
	import std.process : execute;
	import std.typecons : Tuple;
	import std.path : buildPath;
	import handle : GetRTXmlResponse;

	auto stm = client.Stm;
	ubyte[] toWrite;
	auto tHash = toHexString(StreamRead(client.Stm, 20));
	
	auto conn = connectTCP(Config.SCGIHost, Config.SCGIPort);
	if (!conn.connected) {
		WriteIntToBuffer(toWrite, cast(ubyte)0);
		WriteStringToBuffer(toWrite, "Failed to connect to rtorrent");
		stm.write(toWrite);
		return;
	}

	auto xmlNode = GetRTXmlResponse(conn, "<?xml version=\"1.0\" ?><methodCall><methodName>d.base_path</methodName><params><param><value><string>" ~ tHash ~ "</string></value></param></params></methodCall>");

	auto dir = decode(xmlNode.parseXPath(XPaths.SINGLE_RESPONSE)[0].getInnerXML());

	auto singleFile = ReadInt!bool(stm);

	string path = dir;

	if (!singleFile)
		path = buildPath(dir, ReadString(stm));

	string ret;
	bool ok = false;

	Tuple!(int, "status", string, "output") mediainfo;
	try {
		mediainfo = execute([ "mediainfo", path ]);
		if (mediainfo.status == 0) {
			ret = mediainfo.output;
			ok = true;
		} else
			ret = "Mediainfo exited with status " ~ to!string(mediainfo.status);
	} catch (Exception ex) {
		ret = "Error. " ~ ex.msg;
	}
	
	WriteIntToBuffer(toWrite, ok);
	WriteStringToBuffer(toWrite, ret);
	stm.write(toWrite);
}

void ActDownloadFile(ref Client client, XmlNode ResXml)
{
	import std.path : buildPath;
	import handle : GetRTXmlResponse;

	auto stm = client.Stm;
	ubyte[] toWrite;
	auto tHash = toHexString(StreamRead(client.Stm, 20));

	auto conn = connectTCP(Config.SCGIHost, Config.SCGIPort);
	if (!conn.connected) {
		WriteIntToBuffer(toWrite, cast(ubyte)0);
		WriteStringToBuffer(toWrite, "Failed to connect to rtorrent");
		stm.write(toWrite);
		return;
	}

	auto xmlNode = GetRTXmlResponse(conn, "<?xml version=\"1.0\" ?><methodCall><methodName>d.base_path</methodName><params><param><value><string>" ~ tHash ~ "</string></value></param></params></methodCall>");

	auto dir = decode(xmlNode.parseXPath(XPaths.SINGLE_RESPONSE)[0].getInnerXML());

	auto singleFile = ReadInt!bool(stm);

	string path = dir;

	if (!singleFile)
		path = buildPath(dir, ReadString(stm));

	FileStream fs;

	logInfo("open %s", path);

	try fs = openFile(path, FileMode.read);
    catch (Exception ex) {
		logInfo("EX %s", ex.msg);

        WriteIntToBuffer(toWrite, cast(ubyte)0);
        WriteStringToBuffer(toWrite, ex.msg);
		stm.write(toWrite);
        return;
    }

    WriteIntToBuffer(toWrite, cast(ubyte)1);
	WriteIntToBuffer(toWrite, cast(ulong)fs.size);
	logInfo("size %d", fs.size);
    stm.write(toWrite);

    try stm.write(fs, fs.size);
    catch (Exception ex) {
        logWarn("Exception at ActDownloadFile while uploading file: %s", ex.msg);
    }
	logInfo("Done.");
}

void ActEditTorrent(ref Client client, XmlNode ResXml)
{
	// TODO: report of errors
}

void ActDirectoryListing(ref Client client)
{
	import std.file : dirEntries, DirEntry;

	ubyte[] toWrite;
	DirEntry[] dirs;

	try {
		auto path = ReadString(client.Stm);
		dirs = dirEntries(path, SpanMode.shallow)
			.filter!(x => x.isDir)
			.array();
	} catch (Exception ex) {
		WriteIntToBuffer(toWrite, false);
		WriteStringToBuffer(toWrite, ex.msg);
		client.Stm.write(toWrite);
		return;
	}

	WriteIntToBuffer(toWrite, true);
	WriteIntToBuffer(toWrite, cast(uint)dirs.length);

	foreach (DirEntry dir; dirs) {
		WriteStringToBuffer(toWrite, dir.name); logInfo("W %s", dir.name); }
	client.Stm.write(toWrite);
}

void ActCreateDirectory(ref Client client)
{
	import std.file : mkdir;

	mkdir(ReadString(client.Stm));

	// OK_HAND
}

void ActIFastResumeGetMTimes(ref Client client)
{
	import std.file : timeLastModified;

	auto stm = client.Stm;
	auto count = ReadInt!uint(stm);
	ubyte[] toWrite;

	for (auto x = 0;x < count;x++) {
		auto path = ReadString(stm);
		SysTime time;

		try	time = timeLastModified(path);
		catch (Exception ex) {
			logWarn("Failed to get modified time on ActIFastResumeGetMTimes, path %s", path);
			WriteIntToBuffer(toWrite, cast(ulong)0);
			continue;
		}

		WriteIntToBuffer(toWrite, time.toUnixTime!long());
	}

	stm.write(toWrite);
}

void ActLoadString(ref Client client, XmlNode ResXml)
{
	logInfo("LoadString: %s", ResXml.toString());
	// TODO: report of errors
}