module handle;

import std.bitmanip;
import std.format;
import std.algorithm;
import std.algorithm.comparison;
import std.socket;
import std.typecons;
import std.datetime;
import std.conv;
import std.container;
import std.range;

import config;
import xpaths;
import sendEx;
import xmls;
import actions;

import vibe.d;
import kxml.xml;

void hexDump(ubyte[] data, uint mark_pos = 0)
{
	try {
		auto writer = appender!string();
		formattedWrite(writer, "Length: %d bytes", data.length);
		if(mark_pos) {
			if(mark_pos < data.length) {
				formattedWrite(writer, "; Mark Byte: %d, Line %d", mark_pos, mark_pos/16);
			} else {
				formattedWrite(writer, "; Mark: [out of range]");
			}
		} else {
			mark_pos = cast(uint)data.length;
		}
		formattedWrite(writer, "\n");

		//convert byte to hex
		char[2] hex;
		auto toHex = function (ubyte x, char[2] hex)
		{
			static const char[16] char_array = "0123456789abcdef";
			hex[0] = char_array[(x >> 4)];
			hex[1] = char_array[(x & 0xF)];
		};

		//print all full lines
		uint pos = 0;
		for(ushort line = 0; line < data.length/16; line++)
		{
			//print current position
			formattedWrite(writer, "%d", pos);
			formattedWrite(writer, "\t");

			for (ubyte i = 0; i < 8; i++, pos++) {
				formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
				ubyte c =  data[pos];
				//toHex(c, hex);
				formattedWrite(writer, "%02x", data[pos]);
			}
			formattedWrite(writer, " ");
			for (ubyte i = 0; i < 8; i++, pos++) {
				formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
				ubyte c =  data[pos];
				//toHex(c, hex);
				formattedWrite(writer, "%02x", data[pos]);
			}

			formattedWrite(writer, "  |");
			pos -= 16;

			for (ubyte i = 0; i < 8; i++, pos++) {
				char c = data[pos];
				formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
			}
			formattedWrite(writer, " ");
			for (ubyte i = 0; i < 8; i++, pos++) {
				char c = data[pos];
				formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
			}
			formattedWrite(writer, "|\n");
		}

		uint lpos = cast(uint)(data.length - pos);
		if(lpos == 0) return;

		formattedWrite(writer, "%d", pos);
		formattedWrite(writer, "\t");

		if(lpos < 9) //print lines with less then 9 hex values
		{
			for (ubyte i = 0; i < lpos; i++, pos++) {
				formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
				ubyte c =  data[pos];
				//toHex(c, hex);
				formattedWrite(writer, "%02x", data[pos]);
			}

			//padding for hex field
			for (ubyte i = 0; i < (3 * 16) - (3 * lpos) + 1; i++) {
				formattedWrite(writer, " ");
			}

			formattedWrite(writer, "  |");
			pos -= lpos;

			for (ubyte i = 0; i < lpos; i++, pos++) {
				char c = data[pos];
				formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
			}
			formattedWrite(writer, " ");

			//padding for text field
			for (ubyte i = 0; i < 16 - lpos; i++) {
				formattedWrite(writer, " ");
			}
			formattedWrite(writer, "|\n");
		}
		else //print lines with less then 16 hex values
		{
			for (ubyte i = 0; i < 8; i++, pos++) {
				formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
				ubyte c =  data[pos];
				//toHex(c, hex);
				formattedWrite(writer, "%02x", data[pos]);
			}
			formattedWrite(writer, " ");
			for (ubyte i = 0; i < lpos - 8; i++, pos++) {
				formattedWrite(writer, "%s", (mark_pos == pos) ? "|" : " ");
				ubyte c =  data[pos];
				//toHex(c, hex);
				formattedWrite(writer, "%02x", data[pos]);
			}
			//padding for hex field
			for (ubyte i = 0; i < (3 * 16) - (3 * lpos); i++) {
				formattedWrite(writer, " ");
			}

			formattedWrite(writer, "  |");
			pos -= lpos;

			for (ubyte i = 0; i < 8; i++, pos++) {
				char c = data[pos];
				formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
			}
			formattedWrite(writer, " ");
			for (ubyte i = 0; i < lpos - 8; i++, pos++) {
				char c = data[pos];
				formattedWrite(writer, "%c", (c > 32 && c < 127) ? c : '.');
			}

			//padding for text field
			for (ubyte i = 0; i < 16 - lpos; i++) {
				formattedWrite(writer, " ");
			}
			formattedWrite(writer, "|\n");
		}
		logInfo(writer.data);
	} catch {
	}
}

XmlNode GetRTXmlResponse(TCPConnection RTConnection, string Xml)
{
    string mid = format("CONTENT_LENGTH\x00%d\x00SCGI\x00" ~ "1\x00", Xml.length);
    string all = format("%d:%s,%s", mid.length, mid, Xml);
	string res;

	try {
		debug logTrace("rtorrent write %s", all);

		RTConnection.write(all);
		res = cast(string)readUntil(RTConnection, cast(ubyte[])("</methodResponse>\r\n"));

		debug logTrace("rtorrent response %s", res);
	} catch (Exception ex) {
		logWarn("Failed to communicate with rtorrent: %s", ex.msg);
		logDiagnostic("Tried to write: %s", all);
	}
    res ~= "</methodResponse>\r\n";
    auto headerEnd = indexOf(res, "\r\n\r\n");

    debug(finalXml) std.file.write("xml.xml", res[headerEnd + 4..$]);

    return readDocument(res[headerEnd + 4..$]);
}

void HandleClient(ref Client client)
{
    auto stm = client.Stm;

    while (1) {
	    SCGI_ACTION action = cast(SCGI_ACTION)(ReadInt!ubyte(stm));

        string xml = "";
        XML_RET xmlRet;
        XmlNode resXml = null;
        ptrdiff_t headerEnd;
        ubyte[] res;

		TCPConnection conn;

		logDiagnostic("ACTION: %d", action);

	    /*final*/ switch (action) {
			case SCGI_ACTION.VERSION:
				break;
		    case SCGI_ACTION.GET_ALL_TORRENT_ROWS:
                xmlRet = XmlGetTorrents(client);
			    break;
            case SCGI_ACTION.LOAD_TORRENT_RAW:
                xmlRet = XmlLoadRaw(client);
			    break;
            case SCGI_ACTION.START_TORRENTS_MULTI:
                xmlRet = XmlStartTorrentsMulti(client);
                break;
            case SCGI_ACTION.STOP_TORRENTS_MULTI:
                xmlRet = XmlStopTorrentsMulti(client);
                break;
            case SCGI_ACTION.PAUSE_TORRENTS_MULTI:
                xmlRet = XmlPauseTorrentsMulti(client);
                break;
            case SCGI_ACTION.FORCE_RECHECK_TORRENTS_MULTI:
                xmlRet = XmlForceRecheckTorrentsMulti(client);
                break;
            case SCGI_ACTION.REANNOUNCE_TORRENTS_MULTI:
                xmlRet = XmlReannounceTorrentsMulti(client);
                break;
            case SCGI_ACTION.ADD_PEER_TORRENT:
                xmlRet = XmlAddPeerTorrent(client);
                break;
            case SCGI_ACTION.SET_LABEL_TORRENTS_MULTI:
                xmlRet = XmlSetLabelTorrentsMulti(client);
                break;
            case SCGI_ACTION.SET_PRIORITY_TORRENTS_MULTI:
                xmlRet = XmlSetPriorityTorrentsMulti(client);
                break;
            case SCGI_ACTION.REMOVE_TORRENTS_MULTI:
                xmlRet = XmlRemoveTorrentsMulti(client);
                break;
            case SCGI_ACTION.REMOVE_TORRENTS_PLUS_DATA_MULTI:
                xmlRet = XmlRemoveTorrentsPlusDataMulti(client);
                break;
            case SCGI_ACTION.GET_TORRENT_FILES_MULTI:
                xmlRet = XmlGetTorrentFilesMulti(client, conn);
                break;
            case SCGI_ACTION.GET_TORRENT_PEERS:
                xmlRet = XmlGetTorrentPeers(client);
                break;
			case SCGI_ACTION.GET_TORRENT_FILES:
                xmlRet = XmlGetTorrentFiles(client);
                break;
			case SCGI_ACTION.CUSTOM_CMD:
				xmlRet = XmlExecCustomCommand(client);
				break;
			case SCGI_ACTION.GET_SETTINGS:
				xmlRet = XmlGetSettings(client);
				break;
			case SCGI_ACTION.SET_SETTINGS:
				xmlRet = XmlSetSettings(client);
				break;
            case SCGI_ACTION.TRANSFER_FILE:
                xmlRet = XmlTransferFile();
                break;
			case SCGI_ACTION.TOGGLE_TRACKER:
				xmlRet = XmlToggleTracker(client);
				break;
			case SCGI_ACTION.KICK_PEER:
				xmlRet = XmlKickPeer(client);
				break;
			case SCGI_ACTION.BAN_PEER:
				xmlRet = XmlBanPeer(client);
				break;
			case SCGI_ACTION.TOGGLE_SNUB_PEER:
				xmlRet = XmlToggleSnubPeer(client);
				break;
			case SCGI_ACTION.SET_FILE_PRIORITY:
				xmlRet = XmlSetFilePriority(client);
				break;
			case SCGI_ACTION.SET_FILE_DOWNLOAD_STRATEGY:
				xmlRet = XmlSetFileDownloadStrategy(client);
				break;
			case SCGI_ACTION.FILE_MEDIAINFO:
				xmlRet = XmlFileMediaInfo(client);
				break;
			case SCGI_ACTION.DOWNLOAD_FILE:
				xmlRet = XmlDownloadFile(client);
				break;
			case SCGI_ACTION.EDIT_TORRENT:
				xmlRet = XmlEditTorrent(client);
				break;
			case SCGI_ACTION.LIST_DIRECTORIES:
				xmlRet = XmlDirectoryListing(client);
				break;
			case SCGI_ACTION.CREATE_DIRECTORY:
				xmlRet = XmlCreateDirectory(client);
				break;
			case SCGI_ACTION.I_FR_GET_MTIMES:
				xmlRet = XmlIFastResumeGetMTimes(client);
				break;
			case SCGI_ACTION.LOAD_TORRENT_STRING:
				xmlRet = XmlLoadString(client);
				break;
		    default:
			    goto G_nextCommand;
	    }
		xml = xmlRet.Xml;

		if (xml.length != 0) {
			conn = connectTCP(Config.SCGIHost, Config.SCGIPort);
			if (!conn.connected) {
				logWarn("Could not connect to rtorrent");
				return;
			}
			resXml = GetRTXmlResponse(conn, xml);
		}

	    /*final*/ switch (action) {
			case SCGI_ACTION.VERSION:
				ActVersion(client);
				break;
		    case SCGI_ACTION.GET_ALL_TORRENT_ROWS:
                ActGetTorrents(client, xmlRet.Data, resXml);
			    break;
            case SCGI_ACTION.LOAD_TORRENT_RAW:
                ActLoadRaw(client, resXml);
			    break;
            case SCGI_ACTION.START_TORRENTS_MULTI:
                ActStartTorrentsMulti(client, resXml);
                break;
            case SCGI_ACTION.STOP_TORRENTS_MULTI:
                ActStopTorrentsMulti(client, resXml);
                break;
            case SCGI_ACTION.PAUSE_TORRENTS_MULTI:
                ActPauseTorrentsMulti(client, resXml);
                break;
            case SCGI_ACTION.FORCE_RECHECK_TORRENTS_MULTI:
                ActForceRecheckTorrentsMulti(client, resXml);
                break;
            case SCGI_ACTION.REANNOUNCE_TORRENTS_MULTI:
                ActReannounceTorrentsMulti(client, resXml);
                break;
            case SCGI_ACTION.ADD_PEER_TORRENT:
                ActAddPeerTorrent(client, resXml);
                break;
            case SCGI_ACTION.SET_LABEL_TORRENTS_MULTI:
                ActSetLabelTorrentsMulti(client, resXml);
                break;
            case SCGI_ACTION.SET_PRIORITY_TORRENTS_MULTI:
                ActSetPriorityTorrentsMulti(client, resXml);
                break;
            case SCGI_ACTION.REMOVE_TORRENTS_MULTI:
                ActRemoveTorrentsMulti(client, resXml);
                break;
            case SCGI_ACTION.REMOVE_TORRENTS_PLUS_DATA_MULTI:
                ActRemoveTorrentsPlusDataMulti(client, resXml);
                break;
            case SCGI_ACTION.GET_TORRENT_FILES_MULTI:
                ActGetTorrentFilesMulti(client, xmlRet.Data);
                break;
            case SCGI_ACTION.GET_TORRENT_PEERS:
                ActGetTorrentPeers(client, xmlRet.Data, resXml);
                break;
			case SCGI_ACTION.GET_TORRENT_FILES:
				ActGetTorrentFiles(client, xmlRet.Data, resXml);
                break;
			case SCGI_ACTION.CUSTOM_CMD:
				ActExecCustomCommand(client, resXml);
				break;
			case SCGI_ACTION.GET_SETTINGS:
				ActGetSettings(client, resXml);
				break;
            case SCGI_ACTION.SET_SETTINGS:
                ActSetSettings(client, resXml);
                break;
            case SCGI_ACTION.TRANSFER_FILE:
                ActTransferFile(client);
                break;
			case SCGI_ACTION.TOGGLE_TRACKER:
				ActToggleTracker(client, resXml);
				break;
			case SCGI_ACTION.KICK_PEER:
				ActKickPeer(client, resXml);
				break;
			case SCGI_ACTION.BAN_PEER:
				ActBanPeer(client, resXml);
				break;
			case SCGI_ACTION.TOGGLE_SNUB_PEER:
				ActToggleSnubPeer(client, resXml);
				break;
			case SCGI_ACTION.SET_FILE_PRIORITY:
				ActSetFilePriority(client, resXml);
				break;
			case SCGI_ACTION.SET_FILE_DOWNLOAD_STRATEGY:
				ActSetFileDownloadStrategy(client, resXml);
				break;
			case SCGI_ACTION.FILE_MEDIAINFO:
				ActFileMediaInfo(client, resXml);
				break;
			case SCGI_ACTION.DOWNLOAD_FILE:
				ActDownloadFile(client, resXml);
				break;
			case SCGI_ACTION.EDIT_TORRENT:
				ActEditTorrent(client, resXml);
				break;
			case SCGI_ACTION.LIST_DIRECTORIES:
				ActDirectoryListing(client);
				break;
			case SCGI_ACTION.CREATE_DIRECTORY:
				ActCreateDirectory(client);
				break;
			case SCGI_ACTION.I_FR_GET_MTIMES:
				ActIFastResumeGetMTimes(client);
				break;
			case SCGI_ACTION.LOAD_TORRENT_STRING:
				ActLoadString(client, resXml);
				break;
		    default:
			    break;
	    }

        logDiagnostic("---------------------- command end ----------------------");
        G_nextCommand:;
    }
}
