module xmls;

import std.random;
import std.variant;
import std.base64;
import std.digest.digest;
import std.conv;
import std.path;
import std.file;
import std.container;
import std.array;
import std.xml;
import vibe.d;

import sendEx;
import handle;
import xpaths;

struct XML_RET {
    string Xml;
    Variant[] Data;
}

XML_RET XmlGetTorrents(ref Client client)
{
    auto stm = client.Stm;

    bool sendOnlyUpdate;

    auto recvTag = StreamRead!(uint)(stm);
	
	debug logDebug("Received tag %d", recvTag);

    if (client.LastTag == recvTag && recvTag != 0)
        sendOnlyUpdate = true;

    uint LastLastTag = client.LastTag;
    do {
        client.LastTag = uniform(0, uint.max);
		debug logDebugV("TAG %d", client.LastTag);
    } while (client.LastTag == LastLastTag && client.LastTag != 0);

	debug logDebug("Generated tag %d", client.LastTag);

    ubyte[] tagBuffer;
    WriteBinToBuffer(tagBuffer, client.LastTag);
    stm.write(tagBuffer);

    string xml = "<?xml version=\"1.0\" ?><methodCall><methodName>d.multicall</methodName><params><param><value><string>main</string></value></param><param><value><string>d.get_name=</string></value></param><param><value><string>d.get_size_bytes=</string></value></param><param><value><string>d.get_completed_bytes=</string></value></param><param><value><string>d.get_up_total=</string></value></param><param><value><string>d.get_down_rate=</string></value></param><param><value><string>d.get_up_rate=</string></value></param><param><value><string>d.get_creation_date=</string></value></param><param><value><string>d.get_custom=addtime</string></value></param><param><value><string>d.get_custom=seedingtime</string></value></param><param><value><string>d.get_custom1=</string></value></param><param><value><string>d.get_priority=</string></value></param><param><value><string>d.chunk_size=</string></value></param><param><value><string>cat=\"$t.multicall=d.get_hash=,t.get_url=,cat=@,t.is_enabled=,cat=@,t.get_scrape_complete=,cat=@,t.get_scrape_incomplete=,cat=@,t.get_scrape_downloaded=,cat=@,t.get_scrape_time_last=,cat=@,t.get_normal_interval=,cat=*\"</string></value></param><param><value><string>d.message=</string></value></param><param><value><string>d.is_open=</string></value></param><param><value><string>d.get_state=</string></value></param><param><value><string>d.is_hash_checking=</string></value></param><param><value><string>d.is_private=</string></value></param><param><value><string>d.get_peers_complete=</string></value></param><param><value><string>cat=\"$t.multicall=d.get_hash=,t.get_scrape_complete=,cat={+}\"</string></value></param><param><value><string>d.get_peers_accounted=</string></value></param><param><value><string>cat=\"$t.multicall=d.get_hash=,t.get_scrape_incomplete=,cat={+}\"</string></value></param><param><value><string>d.get_hash=</string></value></param></params></methodCall>";

    XML_RET ret = {
        xml,
        variantArray(
            sendOnlyUpdate,
			23
        )
    };

    return ret;
}

XML_RET XmlLoadRaw(ref Client client)
{
    auto startOnAdd = ReadInt!bool(client.Stm);
    auto path = ReadString(client.Stm);
    auto size = ReadInt!uint(client.Stm);
    auto data = StreamRead(client.Stm, size);

	string xml = "<?xml version=\"1.0\"?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data><value><struct><member><name>methodName</name><value><string>load_raw" ~ (startOnAdd ? "_start" : "") ~ "</string></value></member><member><name>params</name><value><array><data><value><base64>";
	xml ~= Base64.encode(data);
	xml ~= "</base64></value>";

	if (path != "")
		xml ~= "<value><string>d.set_directory=\"" ~ encode(path) ~ "\"</string></value>";

	xml ~= "</data></array></value></member></struct></value></data></array></value></param></params></methodCall>";

	logDiagnostic("LoadRaw XML %s", xml);

    XML_RET ret = { xml, null };
    return ret;
}

private XML_RET XmlActionTorrentsMulti(ref Client client, string[] Actions)
{
    auto count = ReadInt!uint(client.Stm);

    string xml = "<?xml version=\"1.0\"?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>";
    for (auto x = 0;x < count;x++) {
        auto sHash = toHexString(StreamRead(client.Stm, 20));

        foreach (action; Actions) {
            xml ~= "<value><struct><member><name>methodName</name><value><string>" ~ action ~ "</string></value></member><member><name>params</name><value><array><data><value><string>" ~ sHash ~ "</string></value></data></array></value></member></struct></value>";
        }
    }

    xml ~= "</data></array></value></param></params></methodCall>";

    XML_RET ret = { xml, null };
    return ret;
}

XML_RET XmlStartTorrentsMulti(ref Client client)
{
    return XmlActionTorrentsMulti(client, [ "d.open", "d.start" ]);
}

XML_RET XmlStopTorrentsMulti(ref Client client)
{
    return XmlActionTorrentsMulti(client, [ "d.stop", "d.close" ]);
}

XML_RET XmlPauseTorrentsMulti(ref Client client)
{
    return XmlActionTorrentsMulti(client, [ "d.stop" ]);
}

XML_RET XmlForceRecheckTorrentsMulti(ref Client client)
{
    return XmlActionTorrentsMulti(client, [ "d.check_hash" ]);
}

XML_RET XmlReannounceTorrentsMulti(ref Client client)
{
    return XmlActionTorrentsMulti(client, [ "d.tracker_announce" ]);
}

XML_RET XmlAddPeerTorrent(ref Client client)
{
	auto sHash = toHexString(StreamRead(client.Stm, 20));
    auto sIp = ReadString(client.Stm);

    string xml = "<?xml version=\"1.0\"?><methodCall><methodName>add_peer</methodName><params><param><value><string>" ~ sHash ~ "</string></value></param><param><value><string>" ~ encode(sIp) ~ "</string></value></param></params></methodCall>";

    XML_RET ret = { xml, null };
    return ret;
}

XML_RET XmlActionTorrentsMultiArg(ref Client client, string Action, string Arg)
{
    auto count = ReadInt!uint(client.Stm);

    string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>";

    for (auto x = 0;x < count;x++) {
        auto sHash = toHexString(StreamRead(client.Stm, 20));

        xml ~= "<value><struct><member><name>methodName</name><value><string>" ~ Action ~ "</string></value></member><member><name>params</name><value><array><data><value><string>" ~ sHash ~ "</string></value><value>" ~ Arg ~ "</value></data></array></value></member></struct></value>";
    }

    xml ~= "</data></array></value></param></params></methodCall>";

    XML_RET ret = { xml, null };
    return ret;
}

XML_RET XmlSetLabelTorrentsMulti(ref Client client)
{
    auto label = ReadString(client.Stm);

    return XmlActionTorrentsMultiArg(client, "d.set_custom1", "<string>" ~ encode(label) ~ "</string>");
}

XML_RET XmlSetPriorityTorrentsMulti(ref Client client)
{
    auto priority = ReadInt!ubyte(client.Stm);

    return XmlActionTorrentsMultiArg(client, "d.set_priority", "<i4>" ~ to!string(priority) ~ "</i4>");
}

XML_RET XmlRemoveTorrentsMulti(ref Client client)
{
    return XmlActionTorrentsMulti(client, [ "d.erase" ]);
}

XML_RET XmlRemoveTorrentsPlusDataMulti(ref Client client)
{
    auto count = ReadInt!uint(client.Stm);

    string xml = "<?xml version=\"1.0\"?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>";
    for (auto x = 0;x < count;x++) {
        auto sHash = toHexString(StreamRead(client.Stm, 20));

		xml ~= "<value><struct><member><name>methodName</name><value><string>d.base_path</string></value></member><member><name>params</name><value><array><data><value><string>" ~ sHash ~ "</string></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>d.delete_tied</string></value></member><member><name>params</name><value><array><data><value><string>" ~ sHash ~ "</string></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>d.erase</string></value></member><member><name>params</name><value><array><data><value><string>" ~ sHash ~ "</string></value></data></array></value></member></struct></value>";
    }

    xml ~= "</data></array></value></param></params></methodCall>";

    XML_RET ret = { xml, null };
    return ret;
}

string XmlSystemSettings(string[] Settings)
{
    string xml = "<?xml version=\"1.0\"?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>";
    foreach (setting; Settings) {
        xml ~= "<value><struct><member><name>methodName</name><value><string>" ~ setting ~ "</string></value></member><member><name>params</name><value><array><data /></array></value></member></struct></value>";
    }
    xml ~= "</data></array></value></param></params></methodCall>";
    return xml;
}

XML_RET XmlGetTorrentFilesMulti(ref Client client, TCPConnection RTConnection)
{
    string xml = XmlSystemSettings([ "get_session" ]);

    auto xmlNode = GetRTXmlResponse(RTConnection, xml);
    auto fields = xmlNode.parseXPath(XPaths.RT_SETTING_STRING);
    string sessionPath = fields[0].getInnerXML();

    string[] files;

    auto count = ReadInt!uint(client.Stm);
    for (auto x = 0;x < count;x++) {
        auto sHash = toHexString(StreamRead(client.Stm, 20)) ~ ".torrent";

        string torrentPath = buildPath(sessionPath, sHash);
        if (exists(torrentPath))
            files ~= torrentPath;
    }

    XML_RET ret = { null, variantArray(files) };
    return ret;
}

XML_RET XmlGetTorrentPeers(ref Client client)
{
    auto stm = client.Stm;

    string sHash = toHexString(StreamRead(client.Stm, 20));

    string xml = "<?xml version=\"1.0\" ?><methodCall><methodName>p.multicall</methodName><params><param><value><string>" ~ sHash ~ "</string></value></param><param><value><string></string></value></param><param><value><string>p.get_id=</string></value></param><param><value><string>p.get_client_version=</string></value></param><param><value><string>p.get_down_total=</string></value></param><param><value><string>p.get_up_total=</string></value></param><param><value><string>p.get_down_rate=</string></value></param><param><value><string>p.get_up_rate=</string></value></param><param><value><string>p.is_incoming=</string></value></param><param><value><string>p.is_encrypted=</string></value></param><param><value><string>p.is_snubbed=</string></value></param><param><value><string>p.is_obfuscated=</string></value></param><param><value><string>p.get_address=</string></value></param><param><value><string>p.get_port=</string></value></param></params></methodCall>";

    XML_RET ret = { xml, variantArray(12) };

    return ret;
}

XML_RET XmlGetTorrentFiles(ref Client client)
{
    auto stm = client.Stm;

    string sHash = toHexString(StreamRead(client.Stm, 20));

    string xml = "<?xml version=\"1.0\" ?><methodCall><methodName>f.multicall</methodName><params><param><value><string>" ~ sHash ~ "</string></value></param><param><value><string></string></value></param><param><value><string>f.size_bytes=</string></value></param><param><value><string>f.priority=</string></value></param><param><value><string>f.path=</string></value></param><param><value><string>f.completed_chunks=</string></value></param><param><value><string>f.prioritize_first=</string></value></param><param><value><string>f.prioritize_last=</string></value></param></params></methodCall>";

    XML_RET ret = { xml, variantArray(6) };

    return ret;
}

XML_RET XmlExecCustomCommand(ref Client client)
{
    XML_RET ret = { "<?xml version=\"1.0\" ?>" ~ ReadString(client.Stm) };
    return ret;
}

XML_RET XmlGetSettings(ref Client client)
{
	string xml = XmlSystemSettings([
		"get_max_uploads", "get_min_peers", "get_max_peers", "get_min_peers_seed", "get_max_peers_seed", "get_tracker_numwant", "get_check_hash", "get_directory",
		
		"get_port_open", "get_port_range", "get_port_random",
		"get_upload_rate", "get_download_rate",
		"get_max_uploads_global", "get_max_downloads_global", "get_max_memory_usage", "get_max_open_files", "get_max_open_http",
		
		"get_dht_port", "get_peer_exchange", "get_ip",
		
		"get_http_cacert", "get_http_capath",
		"get_max_downloads_div", "get_max_uploads_div", "get_max_file_size",
		"get_preload_type", "get_preload_min_size", "get_preload_required_rate",
		"get_receive_buffer_size", "get_send_buffer_size",
		"get_safe_sync", "get_timeout_safe_sync", "get_timeout_sync",
		"get_scgi_dont_route",
		"get_session", "get_session_lock", "get_session_on_completion",
		"get_split_file_size", "get_split_suffix",
		"get_use_udp_trackers",
		"get_http_proxy", "get_proxy_address", "get_bind"]);

	XML_RET ret = { xml };
	return ret;
}

XML_RET XmlSetSettings(ref Client client)
{
    auto stm = client.Stm;

	Tuple!(string, int)[] settings = [
        tuple("set_max_uploads", 11), // Data type long
        tuple("set_min_peers", 11),
        tuple("set_max_peers", 11),
        tuple("set_min_peers_seed", 11),
        tuple("set_max_peers_seed", 11),
        tuple("set_tracker_numwant", 11),
        tuple("set_check_hash", 3), // Data type bool
        tuple("set_directory", 18), // Data type string
        tuple("set_port_open", 3),
        tuple("set_port_range", 18),
        tuple("set_port_random", 3),
        tuple("set_upload_rate", 11),
        tuple("set_download_rate", 11),
        tuple("set_max_uploads_global", 11),
        tuple("set_max_downloads_global", 11),
        tuple("set_max_memory_usage", 11),
        tuple("set_max_open_files", 11),
        tuple("set_max_open_http", 11),
        tuple("set_dht_port", 11),
        tuple("set_peer_exchange", 3),
        tuple("set_ip", 18),
        tuple("set_http_cacert", 18),
        tuple("set_http_capath", 18),
        tuple("set_max_downloads_div", 11),
        tuple("set_max_uploads_div", 11),
        tuple("set_max_file_size", 11),
        tuple("set_preload_type", 11),
        tuple("set_preload_min_size", 11),
        tuple("set_preload_required_rate", 11),
        tuple("set_receive_buffer_size", 11),
        tuple("set_send_buffer_size", 11),
        tuple("set_safe_sync", 3),
        tuple("set_timeout_safe_sync", 11),
        tuple("set_timeout_sync", 11),
        tuple("set_scgi_dont_route", 3),
        tuple("set_session", 18),
        tuple("set_session_lock", 3),
        tuple("set_session_on_completion", 3),
        tuple("set_split_file_size", 11),
        tuple("set_split_suffix", 18),
        tuple("set_use_udp_trackers", 3),
        tuple("set_http_proxy", 18),
        tuple("set_proxy_address", 18),
        tuple("set_bind", 18)
    ];

    auto xml = "<?xml version=\"1.0\" ?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data>";

    for (auto x = 0;x < settings.length;x++) {
		xml ~= "<value><struct><member><name>methodName</name><value><string>" ~ settings[x][0] ~ "</string></value></member><member><name>params</name><value><array><data><value>";

		string toAdd;
        final switch(settings[x][1]) {
            case 3: // Data type bool
                toAdd = "<i8>" ~ to!string(ReadInt!ubyte(stm)) ~ "</i8>";
                break;
            case 11: // Data type long
				toAdd = "<i8>" ~ to!string(ReadInt!long(stm)) ~ "</i8>";
                break;
            case 18: // Data type string
				toAdd = "<string>" ~ encode(ReadString(stm)) ~ "</string>";
                break;
        }

		xml ~= toAdd;

		logDiagnostic("setting %s type %d data: %s", settings[x][0], settings[x][1], toAdd);

		xml ~= "</value></data></array></value></member></struct></value>";
    }

	xml ~= "</data></array></value></param></params></methodCall>";

	XML_RET ret = { xml };
	return ret;
}

/*XML_RET XmlGetTorrentTrackers(ref Client client)
{
    auto stm = client.Stm;

    auto trackerRows = cast(TRACKER_ROWS)(ReadInt!uint(stm));
    string sHash = toHexString(StreamRead(client.Stm, 20));

    string xml = "<?xml version=\"1.0\" ?><methodCall><methodName>t.multicall</methodName><params><param><value><string>" ~ sHash ~ "</string></value></param><param><value><string></string></value></param>";
    if (trackerRows & TRACKER_ROWS.URI)
        xml ~= "<param><value><string>t.get_url=</string></value></param>";
    if (trackerRows & TRACKER_ROWS.ENABLED)
        xml ~= "<param><value><string>t.is_enabled=</string></value></param>";
    if (trackerRows & TRACKER_ROWS.SEEDERS)
        xml ~= "<param><value><string>t.get_scrape_complete=</string></value></param>";
    if (trackerRows & TRACKER_ROWS.PEERS)
        xml ~= "<param><value><string>t.get_scrape_incomplete=</string></value></param>";
    if (trackerRows & TRACKER_ROWS.DOWNLOADED)
        xml ~= "<param><value><string>t.get_scrape_downloaded=</string></value></param>";
    if (trackerRows & TRACKER_ROWS.LAST_UPDATED)
        xml ~= "<param><value><string>t.get_scrape_time_last=</string></value></param>";
    if (trackerRows & TRACKER_ROWS.INTERVAL)
        xml ~= "<param><value><string>t.get_normal_interval=</string></value></param>";

    xml ~= "</params></methodCall>";

    XML_RET ret = { xml, variantArray(trackerRows) };

    return ret;
}*/

XML_RET XmlTransferFile()
{
    XML_RET ret = { "" };

    return ret;
}

XML_RET XmlToggleTracker(ref Client client)
{
	auto stm = client.Stm;

	auto hash = toHexString(StreamRead(client.Stm, 20));
	auto index = ReadInt!ushort(client.Stm);
	auto enable = ReadInt!ubyte(client.Stm);

	XML_RET ret = { "<?xml version=\"1.0\" ?><methodCall><methodName>t.set_enabled</methodName><params><param><value><string>" ~ hash ~ "</string></value></param><param><value><i4>" ~ to!string(index) ~ "</i4></value></param><param><value><i4>" ~ to!string(enable) ~ "</i4></value></param></params></methodCall>" };

	return ret;
}

XML_RET XmlKickPeer(ref Client client)
{
	auto tHash = toHexString(StreamRead(client.Stm, 20));
	auto pHash = toHexString(StreamRead(client.Stm, 20));

	XML_RET ret = { "<?xml version=\"1.0\" ?><methodCall><methodName>p.disconnect</methodName><params><param><value><string>" ~ tHash ~ ":p" ~ pHash ~ "</string></value></param></params></methodCall>" };

	return ret;
}

XML_RET XmlBanPeer(ref Client client)
{
	auto tHash = toHexString(StreamRead(client.Stm, 20));
	auto pHash = toHexString(StreamRead(client.Stm, 20));

	auto ps = tHash ~ ":p" ~ pHash;

	XML_RET ret = { "<?xml version=\"1.0\" ?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data><value><struct><member><name>methodName</name><value><string>p.banned.set</string></value></member><member><name>params</name><value><array><data><value><string>" ~ ps ~ "</string></value><value><i4>1</i4></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>p.disconnect</string></value></member><member><name>params</name><value><array><data><value><string>" ~ ps ~ "</string></value></data></array></value></member></struct></value></data></array></value></param></params></methodCall>" };

	return ret;
}

XML_RET XmlToggleSnubPeer(ref Client client)
{
	auto tHash = toHexString(StreamRead(client.Stm, 20));
	auto pHash = toHexString(StreamRead(client.Stm, 20));
	auto snub = ReadInt!ubyte(client.Stm);

	XML_RET ret = { "<?xml version=\"1.0\" ?><methodCall><methodName>p.snubbed.set</methodName><params><param><value><string>" ~ tHash ~ ":p" ~ pHash ~ "</string></value></param><param><value><i4>" ~ to!string(snub) ~ "</i4></value></param></params></methodCall>" };

	return ret;
}

XML_RET XmlSetFilePriority(ref Client client)
{
	auto hash = toHexString(StreamRead(client.Stm, 20));
	auto index = ReadInt!uint(client.Stm);
	auto priority = ReadInt!ubyte(client.Stm);

	XML_RET ret = { "<?xml version=\"1.0\" ?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data><value><struct><member><name>methodName</name><value><string>f.set_priority</string></value></member><member><name>params</name><value><array><data><value><string>" ~ hash ~ "</string></value><value><i4>" ~ to!string(index) ~ "</i4></value><value><i4>" ~ to!string(priority) ~ "</i4></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>d.update_priorities</string></value></member><member><name>params</name><value><array><data><value><string>" ~ hash ~ "</string></value></data></array></value></member></struct></value></data></array></value></param></params></methodCall>" };

	return ret;
}

XML_RET XmlSetFileDownloadStrategy(ref Client client)
{
	auto hash = toHexString(StreamRead(client.Stm, 20));
	auto index = ReadInt!uint(client.Stm);
	auto dlStrategy = cast(FILE_DOWNLOAD_STRATEGY)ReadInt!ubyte(client.Stm);

	auto file = hash ~ ":f" ~ to!string(index);

	XML_RET ret = { "<?xml version=\"1.0\" ?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data><value><struct><member><name>methodName</name><value><string>f.prioritize_first." ~
	((dlStrategy == FILE_DOWNLOAD_STRATEGY.NORMAL || dlStrategy == FILE_DOWNLOAD_STRATEGY.TRAILING_CHUCK_FIRST) ? "dis" : "en") ~
	"able</string></value></member><member><name>params</name><value><array><data><value><string>" ~ file ~ "</string></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>f.prioritize_last." ~
	((dlStrategy == FILE_DOWNLOAD_STRATEGY.NORMAL || dlStrategy == FILE_DOWNLOAD_STRATEGY.LEADING_CHUNK_FIRST) ? "dis" : "en") ~
	"able</string></value></member><member><name>params</name><value><array><data><value><string>" ~ file ~ "</string></value></data></array></value></member></struct></value><value><struct><member><name>methodName</name><value><string>d.update_priorities</string></value></member><member><name>params</name><value><array><data><value><string>" ~ hash ~ "</string></value></data></array></value></member></struct></value></data></array></value></param></params></methodCall>" };

	return ret;
}

XML_RET XmlFileMediaInfo(ref Client client)
{
	XML_RET ret = { "" };

    return ret;
}

XML_RET XmlDownloadFile(ref Client client)
{
	XML_RET ret = { "" };

    return ret;
}

XML_RET XmlEditTorrent(ref Client client)
{
	XML_RET ret = { "" };

    return ret;
}

XML_RET XmlDirectoryListing(ref Client client)
{
	XML_RET ret = { "" };

    return ret;
}

XML_RET XmlCreateDirectory(ref Client client)
{
	XML_RET ret = { "" };

    return ret;
}

XML_RET XmlIFastResumeGetMTimes(ref Client client)
{
	XML_RET ret = { "" };

	return ret;
}

XML_RET XmlLoadString(ref Client client)
{
    auto startOnAdd = ReadInt!bool(client.Stm);
    auto path = ReadString(client.Stm);
    auto str = ReadString(client.Stm);

	string xml = "<?xml version=\"1.0\"?><methodCall><methodName>system.multicall</methodName><params><param><value><array><data><value><struct><member><name>methodName</name><value><string>load" ~ (startOnAdd ? "_start" : "") ~ "</string></value></member><member><name>params</name><value><array><data><value><string>" ~ encode(str) ~ "</string></value>";
	if (path != "")
		xml ~= "<value><string>d.set_directory=\"" ~ encode(path) ~ "\"</string></value>";

	xml ~= "</data></array></value></member></struct></value></data></array></value></param></params></methodCall>";

	logDiagnostic("LoadString XML %s", xml);

    XML_RET ret = { xml, null };
    return ret;
}