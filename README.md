# RTSharp-Relay-SDS (alpha, all releases untested)
RTSharp-Relay-SDS is a System Data Supplier plugin for RT# which communicates with RTSharpRelay to expose **rtorrent** interface to core application.
RTSharpRelay is written in [D](https://dlang.org/) and needs to be built from source.

## Building RTSharpRelay from source
Clone the RTSharpRelay and install `dub` (the D package manager) and `dmd` (the D compiler) from [APT repository](http://d-apt.sourceforge.net/) or build them from source if you're feeling adventurous. Build the application:
```
dub build
```
## Configuring RTSharpRelay
The default configuration file (`config.conf`) **must** be changed. Configure server port, access password (minimum 15 chars, use a random password generator), rtorrent SCGI host, rtorrent SCGI port and SSL if you want it enabled.
**Change the access password or the server will not start. USE A STRONG PASSWORD. [Compromised access to RTSharpRelay server is a compromised access to your whole server](https://github.com/rakshasa/rtorrent/wiki/rTorrent-0.9-Comprehensive-Command-list-(WIP)#execution).**

Upon running RT# with RTSharp-Relay-SDS, you will be prompted to enter almost same settings as you've entered in RTSharpRelay `config.conf`, other settings can be left as set by default.