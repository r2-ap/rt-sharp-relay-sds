﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RTSharpIFace;

namespace RTSharp_Relay_SDS
{
	public partial class CustomCommand : Form
	{
		readonly IRTSharp Host;
		readonly IPlugin Self;
		public CustomCommand(IRTSharp Host, IPlugin Self)
		{
			this.Host = Host;
			this.Self = Self;
			InitializeComponent();
		}

		private async void b_exec_Click(object sender, EventArgs e)
		{
			b_exec.Enabled = false;
			var cmd = t_command.Text;
			string ret = await Self.CustomAccess(cmd);
			t_result.Text = ret;
			b_exec.Enabled = true;
		}

		private void b_save_Click(object sender, EventArgs e) => Close();

		private void b_close_Click(object sender, EventArgs e) => Close();
	}
}
