﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RTSharpIFace;

namespace RTSharp_Relay_SDS
{
	public partial class Settings : Form
	{
		IRTSharp Host;
		IPlugin Self;
		public Settings(IRTSharp Host, IPlugin Self)
		{
			this.Host = Host;
			this.Self = Self;
			InitializeComponent();
		}

		private void Settings_Load(object sender, EventArgs e)
		{
			var ini = Host.GetPluginSettings();
			var settings = ini.Sections["SDS-Relay"].Keys;
			t_host.Text = settings["Host"]?.Value ?? "";
			num_port.Value = Int16.Parse(settings["Port"]?.Value ?? "0");
			t_password.Text = settings["Password"]?.Value ?? "";
			num_timeout.Value = Int32.Parse(settings["Timeout"]?.Value ?? "10000");
			num_connections.Value = Int16.Parse(settings["Connections"]?.Value ?? "5");
			chk_reconnect.Checked = settings["AutoReconnect"]?.Value == "1";
		}

		void Apply()
		{
			var ini = Host.GetPluginSettings();
			var settings = ini.Sections["SDS-Relay"].Keys;

			if (num_port.Value == 0) {
				MessageBox.Show("\"0\" is not a valid value for port", "RTSharp-Relay-SDS", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			if (String.IsNullOrEmpty(t_password.Text)) {
				MessageBox.Show("\"\" is not a valid value for password", "RTSharp-Relay-SDS", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			
			if (!settings.Contains("Host"))
				settings.Add("Host");
			if (!settings.Contains("Port"))
				settings.Add("Port");
			if (!settings.Contains("Password"))
				settings.Add("Password");
			if (!settings.Contains("Timeout"))
				settings.Add("Timeout");
			if (!settings.Contains("Connections"))
				settings.Add("Connections");
			if (!settings.Contains("AutoReconnect"))
				settings.Add("AutoReconnect");

			settings["Host"].Value = t_host.Text;
			settings["Port"].Value = num_port.Value.ToString();
			settings["Password"].Value = t_password.Text;
			settings["Timeout"].Value = num_timeout.Value.ToString();
			settings["Connections"].Value = num_connections.Value.ToString();
			settings["AutoReconnect"].Value = chk_reconnect.Checked ? "1" : "0";

			Host.SavePluginSettings(ini);
		}

		private void b_apply_Click(object sender, EventArgs e)
		{
			Apply();
		}

		private void b_ok_Click(object sender, EventArgs e)
		{
			Apply();
			Close();
		}

		private void b_cancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void b_customCmd_Click(object sender, EventArgs e)
		{
			new CustomCommand(Host, Self).Show();
		}
	}
}
