﻿namespace RTSharp_Relay_SDS
{
	partial class Settings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.num_port = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.t_password = new System.Windows.Forms.TextBox();
			this.t_host = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.num_connections = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.num_timeout = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.b_apply = new System.Windows.Forms.Button();
			this.b_cancel = new System.Windows.Forms.Button();
			this.b_ok = new System.Windows.Forms.Button();
			this.b_customCmd = new System.Windows.Forms.Button();
			this.chk_reconnect = new System.Windows.Forms.CheckBox();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.num_port)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.num_connections)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.num_timeout)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.num_port);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.t_password);
			this.groupBox1.Controls.Add(this.t_host);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(177, 97);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Server";
			// 
			// num_port
			// 
			this.num_port.Location = new System.Drawing.Point(44, 45);
			this.num_port.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.num_port.Name = "num_port";
			this.num_port.Size = new System.Drawing.Size(127, 20);
			this.num_port.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 74);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(59, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Password: ";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(32, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Port: ";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Host: ";
			// 
			// t_password
			// 
			this.t_password.Location = new System.Drawing.Point(71, 71);
			this.t_password.Name = "t_password";
			this.t_password.PasswordChar = '•';
			this.t_password.Size = new System.Drawing.Size(100, 20);
			this.t_password.TabIndex = 1;
			// 
			// t_host
			// 
			this.t_host.Location = new System.Drawing.Point(47, 19);
			this.t_host.Name = "t_host";
			this.t_host.Size = new System.Drawing.Size(124, 20);
			this.t_host.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.chk_reconnect);
			this.groupBox2.Controls.Add(this.num_connections);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.num_timeout);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Location = new System.Drawing.Point(195, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(195, 97);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Connection";
			// 
			// num_connections
			// 
			this.num_connections.Location = new System.Drawing.Point(84, 46);
			this.num_connections.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
			this.num_connections.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.num_connections.Name = "num_connections";
			this.num_connections.Size = new System.Drawing.Size(105, 20);
			this.num_connections.TabIndex = 3;
			this.num_connections.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 48);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 13);
			this.label5.TabIndex = 2;
			this.label5.Text = "Connections: ";
			// 
			// num_timeout
			// 
			this.num_timeout.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.num_timeout.Location = new System.Drawing.Point(63, 20);
			this.num_timeout.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
			this.num_timeout.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.num_timeout.Name = "num_timeout";
			this.num_timeout.Size = new System.Drawing.Size(126, 20);
			this.num_timeout.TabIndex = 1;
			this.num_timeout.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 22);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(51, 13);
			this.label4.TabIndex = 0;
			this.label4.Text = "Timeout: ";
			// 
			// b_apply
			// 
			this.b_apply.Location = new System.Drawing.Point(294, 126);
			this.b_apply.Name = "b_apply";
			this.b_apply.Size = new System.Drawing.Size(75, 23);
			this.b_apply.TabIndex = 2;
			this.b_apply.Text = "Apply";
			this.b_apply.UseVisualStyleBackColor = true;
			this.b_apply.Click += new System.EventHandler(this.b_apply_Click);
			// 
			// b_cancel
			// 
			this.b_cancel.Location = new System.Drawing.Point(375, 126);
			this.b_cancel.Name = "b_cancel";
			this.b_cancel.Size = new System.Drawing.Size(75, 23);
			this.b_cancel.TabIndex = 3;
			this.b_cancel.Text = "Cancel";
			this.b_cancel.UseVisualStyleBackColor = true;
			this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
			// 
			// b_ok
			// 
			this.b_ok.Location = new System.Drawing.Point(213, 126);
			this.b_ok.Name = "b_ok";
			this.b_ok.Size = new System.Drawing.Size(75, 23);
			this.b_ok.TabIndex = 4;
			this.b_ok.Text = "OK";
			this.b_ok.UseVisualStyleBackColor = true;
			this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
			// 
			// b_customCmd
			// 
			this.b_customCmd.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.b_customCmd.Location = new System.Drawing.Point(12, 134);
			this.b_customCmd.Name = "b_customCmd";
			this.b_customCmd.Size = new System.Drawing.Size(80, 15);
			this.b_customCmd.TabIndex = 5;
			this.b_customCmd.Text = "Custom command";
			this.b_customCmd.UseVisualStyleBackColor = true;
			this.b_customCmd.Click += new System.EventHandler(this.b_customCmd_Click);
			// 
			// chk_reconnect
			// 
			this.chk_reconnect.AutoSize = true;
			this.chk_reconnect.Location = new System.Drawing.Point(90, 72);
			this.chk_reconnect.Name = "chk_reconnect";
			this.chk_reconnect.Size = new System.Drawing.Size(99, 17);
			this.chk_reconnect.TabIndex = 6;
			this.chk_reconnect.Text = "Auto reconnect";
			this.chk_reconnect.UseVisualStyleBackColor = true;
			// 
			// Settings
			// 
			this.AcceptButton = this.b_ok;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(462, 161);
			this.Controls.Add(this.b_customCmd);
			this.Controls.Add(this.b_ok);
			this.Controls.Add(this.b_cancel);
			this.Controls.Add(this.b_apply);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "Settings";
			this.ShowIcon = false;
			this.Text = "RTSharp-Relay-SDS settings";
			this.Load += new System.EventHandler(this.Settings_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.num_port)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.num_connections)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.num_timeout)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox t_password;
		private System.Windows.Forms.TextBox t_host;
		private System.Windows.Forms.NumericUpDown num_port;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.NumericUpDown num_connections;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown num_timeout;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button b_apply;
		private System.Windows.Forms.Button b_cancel;
		private System.Windows.Forms.Button b_ok;
		private System.Windows.Forms.Button b_customCmd;
		private System.Windows.Forms.CheckBox chk_reconnect;
	}
}