﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using RTSharpIFace;
using static RTSharpIFace.Utils;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Reflection;

namespace RTSharp_Relay_SDS {

	public class Main : ISDS {

		public IRTSharp Host;
		public List<Conn> Connections = new List<Conn>();
		public const int BUFFER_SIZE = 4096;
		public const int TORRENT_HASH_LEN = 20;
		public Smaz smaz;

		ulong GlobalDownload, GlobalUpload;
		int ActiveTorrents;
		SemaphoreSlim GetTorrentsLock = new SemaphoreSlim(1);
		
		public class Conn
		{
			public SemaphoreSlim Lock;
			public TcpClient Client;
			public NetworkStream Stream;
			public uint AllTorrentRowsTag;
#if DEBUG
			public ulong LockTimestamp;
			public int ConnectionID;
#endif
		}

		[Serializable]
		internal struct FILE_ID
		{
			internal int id;
			internal string fullPath;

			public FILE_ID(int id, string fullPath) {
				this.id = id;
				this.fullPath = fullPath;
			}
		}

		public enum SCGI_ACTION : byte {
			VERSION = 0,
			GET_ALL_TORRENT_ROWS = 1,
			LOAD_TORRENT_RAW = 2,
			START_TORRENTS_MULTI = 3,
			PAUSE_TORRENTS_MULTI = 4,
			STOP_TORRENTS_MULTI = 5,
			FORCE_RECHECK_TORRENTS_MULTI = 6,
			REANNOUNCE_TORRENTS_MULTI = 7,
			ADD_PEER_TORRENT = 8,
			SET_LABEL_TORRENTS_MULTI = 9,
			SET_PRIORITY_TORRENTS_MULTI = 10,
			REMOVE_TORRENTS_MULTI = 11,
			REMOVE_TORRENTS_PLUS_DATA_MULTI = 12,

			/// <summary>
			/// Gets .torrent files
			/// </summary>
			GET_TORRENT_FILES_MULTI = 13,
			GET_TORRENT_PEERS = 14,

			/// <summary>
			/// Gets list of torrent data files
			/// </summary>
			GET_TORRENT_FILES = 15,
			CUSTOM_CMD = 16,
			GET_SETTINGS = 17,
			SET_SETTINGS = 18,
			TRANSFER_FILE = 19,
			TOGGLE_TRACKER = 20,
			KICK_PEER = 21,
			BAN_PEER = 22,
			TOGGLE_SNUB_PEER = 33,
			SET_FILE_PRIORITY = 34,
			SET_FILE_DOWNLOAD_STRATEGY = 35,
			FILE_MEDIAINFO = 36,
			DOWNLOAD_FILE = 37,
			EDIT_TORRENT = 38,
			LIST_DIRECTORIES = 39,
			CREATE_DIRECTORY = 40,
			I_FR_GET_MTIMES = 41,
			LOAD_TORRENT_STRING = 42
		}

		// {A47C5CE8-3484-4662-AFF6-1BA700AD894B}
		public Guid GUID => new Guid(new byte[] { 0xa4, 0x7c, 0x5c, 0xe8, 0x34, 0x84, 0x46, 0x62, 0xaf, 0xf6, 0x1b, 0xa7, 0x00, 0xad, 0x89, 0x4b });

		public string Name => "RT#-Relay-SystemDataSupplier";
		public string Description => "Gets SCGI data from RT#-Relay format";
		public string Author => "wdd";
		public string Version => "0.1.0";

		private Tuple<ulong, ulong> COMPATIBLE_REMOTE_VERSION = Tuple.Create(4294967296UL, 8589934591UL);

		public Guid UniqueGUID { get; set; }

		long Ping;
		int Timeout;

		string IntVersionToString(ulong Version) => (Version >> 40) + "." + ((Version & 0xFF00000000) >> 32) + "." + (Version & 0xFFFFFFFF);

		public async Task<Conn> GetAvailableConnection()
		{
			int timeout = 0;

			if (Connections.Count == 0)
				return null;

			for (int x = 0;x < Connections.Count;x++) {
				if (!Connections[x].Client.Connected) {
					Task.Run(() => Connect(false, x));
					continue;
				}
			}

			for (;;) {
				foreach (var c in Connections) {
					if (c.Lock.CurrentCount != 1)
						continue;

					await c.Lock.WaitAsync();
#if DEBUG
					c.LockTimestamp = UnixTimeStamp();
#endif
					return c;
				}

				await Task.Delay(100); timeout += 100;

				if (timeout > 30000) {
#if DEBUG
					MessageBox.Show("            WARNING WARNING WARNING\nCONNECTION DE-SYNC AT #" +
						Connections.OrderBy(x => x.LockTimestamp).ToArray()[0].ConnectionID +
						"\nFIRST LOCKED AT " + UnixTimeStampToDateTime(Connections.OrderBy(x => x.LockTimestamp).ToArray()[0].LockTimestamp).ToString("u") +
						"\n\nDETECTED AT " + UnixTimeStampToDateTime(UnixTimeStamp()).ToString("u") + "\n-" +
						UnixTimeStampToDateTime(UnixTimeStamp() - Connections.OrderBy(x => x.LockTimestamp).ToArray()[0].LockTimestamp).ToString("u"), "XXX", MessageBoxButtons.OK, MessageBoxIcon.Stop);
					Debug.Assert(false);
#endif
					return null;
				}
			}
		}

		public void Init(IRTSharp Host, Guid UniqueGUID) {
			this.Host = Host;
			this.UniqueGUID = UniqueGUID;
			smaz = new Smaz();

			var ini = Host.GetPluginSettings();
			if (ini.Sections["SDS-Relay"] == null)
				ini.Sections.Add("SDS-Relay");

			var settings = ini.Sections["SDS-Relay"];

			bool newSettings = false;

			if (!settings.Keys.Contains("Timeout") ||
				!settings.Keys.Contains("Connections") ||
				!settings.Keys.Contains("Password") ||
				!settings.Keys.Contains("Port") ||
				!settings.Keys.Contains("Host"))
				newSettings = true;

			Host.SavePluginSettings(ini);

			if (newSettings)
				GetPluginSettings(this).ShowDialog();

			Timeout = Int32.Parse(settings.Keys["Timeout"].Value);
		}

		public void Unload() {
			foreach (var c in Connections)
				c.Client.Close();
		}

		public async Task<dynamic> CustomAccess(dynamic In)
		{
			if (!(In is string))
				return null;

			var s = (string)In;

			var c = await GetAvailableConnection();
			var stm = c.Stream;

			try {
				await WriteInt(stm, (byte)SCGI_ACTION.CUSTOM_CMD);
				await WriteString(stm, s);

				return await ReadString(stm);
			} catch (Exception ex) {
				return "EXCEPTION: " + ex.Message;
			} finally {
				c.Lock.Release();
			}
		}

		public ContextMenuStrip GetPluginMenu()
		{
			var cms_main = new ContextMenuStrip();
			var toolStripSeparator1 = new ToolStripSeparator();
			var pingToolStripMenuItem = new ToolStripMenuItem();
			var reconnectToolStripMenuItem = new ToolStripMenuItem();

			cms_main.Items.AddRange(new ToolStripItem[] {
			reconnectToolStripMenuItem,
			toolStripSeparator1,
			pingToolStripMenuItem});
			cms_main.Name = "cms_main";
			cms_main.Size = new System.Drawing.Size(153, 76);
			// 
			// reconnectToolStripMenuItem
			// 
			reconnectToolStripMenuItem.Name = "reconnectToolStripMenuItem";
			reconnectToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			reconnectToolStripMenuItem.Text = "Reconnect";
			// 
			// toolStripSeparator1
			// 
			toolStripSeparator1.Name = "toolStripSeparator1";
			toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
			// 
			// pingToolStripMenuItem
			// 
			pingToolStripMenuItem.Name = "pingToolStripMenuItem";
			pingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			pingToolStripMenuItem.Text = "Ping";

			reconnectToolStripMenuItem.Click += async (Sender, Args) => {
				try {
					await Reconnect();
				} catch (Exception ex) {
					Host.SetStatus("Reconnect: " + ex.Message, true);
				}
			};

			pingToolStripMenuItem.Click += async (Sender, Args) => {
				var c = await GetAvailableConnection();
				Stopwatch sw;
				try {
					var stm = c.Stream;
					sw = new Stopwatch();
					sw.Start();
				
					await WriteInt(stm, (byte)SCGI_ACTION.VERSION);
					await ReadInt<uint>(stm);
				} catch (Exception ex) {
					Host.SetStatus("Ping: " + ex.Message, true);
					return;
				} finally {
					if (c != null)
						c.Lock.Release();
				}
				sw.Stop();

				Ping = sw.ElapsedMilliseconds;
			};

			return cms_main;
		}

		public Form GetPluginSettings(IPlugin Self)
		{
			return new Settings(Host, Self);
		}

		static async Task WriteInt<T>(NetworkStream N, T IIn)
		{
			if (IIn is byte || IIn is sbyte) {
				await N.WriteAsync(new byte[] { (byte)(object)IIn }, 0, 1);
			} else if (IIn is bool)
				await N.WriteAsync(new byte[] { (bool)(object)IIn ? (byte)1 : (byte)0 }, 0, 1);
			else if (IIn is ushort || IIn is short) {
				if (ToType<ushort>(IIn) < 255)
					await WriteInt(N, ToType<byte>(IIn));
				else {
					await N.WriteAsync(new[] { (byte)255 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				}
			} else if (IIn is uint || IIn is int) {
				if (ToType<uint>(IIn) < 254)
					await WriteInt(N, ToType<byte>(IIn));
				else if (ToType<uint>(IIn) < UInt16.MaxValue) {
					await N.WriteAsync(new[] { (byte)254 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				} else {
					await N.WriteAsync(new[] { (byte)255 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<uint>(IIn)), 0, 4);
				}
			} else if (IIn is ulong || IIn is long) {
				if (ToType<ulong>(IIn) < 253)
					await WriteInt(N, ToType<byte>(IIn));
				else if (ToType<ulong>(IIn) < UInt16.MaxValue) {
					await N.WriteAsync(new[] { (byte)253 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<ushort>(IIn)), 0, 2);
				} else if (ToType<ulong>(IIn) < UInt32.MaxValue) {
					await N.WriteAsync(new[] { (byte)254 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<uint>(IIn)), 0, 4);
				} else {
					await N.WriteAsync(new[] { (byte)255 }, 0, 1);
					await N.WriteAsync(GetBytesNetworkOrder(ToType<ulong>(IIn)), 0, 8);
				}
			}
		}

		static async Task<T> ReadInt<T>(NetworkStream In)
		{
			var first = (await StmRead(In, 1))[0];

			if (typeof(T) == typeof(byte))
				return ToType<T>(first);
			else if (typeof(T) == typeof(bool))
				return (T)(object)(first == 1);
			else if (typeof(T) == typeof(ushort) || typeof(T) == typeof(short)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(await StmRead(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			} else if (typeof(T) == typeof(uint) || typeof(T) == typeof(int)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt32(ArrangeBytesNetworkOrder(await StmRead(In, sizeof(uint))), 0));
				else if (first == 254)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(await StmRead(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			} else if (typeof(T) == typeof(ulong) || typeof(T) == typeof(long)) {
				if (first == 255)
					return ToType<T>(BitConverter.ToUInt64(ArrangeBytesNetworkOrder(await StmRead(In, sizeof(ulong))), 0));
				else if (first == 254)
					return ToType<T>(BitConverter.ToUInt32(ArrangeBytesNetworkOrder(await StmRead(In, sizeof(uint))), 0));
				else if (first == 253)
					return ToType<T>(BitConverter.ToUInt16(ArrangeBytesNetworkOrder(await StmRead(In, sizeof(ushort))), 0));
				else
					return ToType<T>(first);
			}

			throw new SocketException();
		}

		static async Task WriteString(NetworkStream In, string SIn)
		{
			await WriteInt(In, SIn.Length);
			await In.WriteAsync(Encoding.UTF8.GetBytes(SIn), 0, SIn.Length);
		}

		static async Task<string> ReadString(NetworkStream In)
		{
			var len = await ReadInt<uint>(In);
			if (len == 0) return "";
			return Encoding.UTF8.GetString(await StmRead(In, (int)len, false));
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static T ToType<T>(dynamic In)
		{
			//return Convert.ChangeType(In, typeof(T));
			return (T)In;
		}

		static async Task<byte[]> StmRead(NetworkStream In, int Size, bool NetworkReorder = true) {
			byte[] buff = new byte[Size];
			int bytes = 0;
			do {
				bytes += await In.ReadAsync(buff, bytes, Size - bytes);
				if (bytes == 0)
					throw new IOException("Received 0 bytes, connection lost.");
			} while (bytes != Size);
			return NetworkReorder ? ReorderBytes(buff) : buff;
		}

		static byte[] ArrangeBytesNetworkOrder(byte[] In)
		{
			if (BitConverter.IsLittleEndian)
				return In.Reverse().ToArray();
			return In;
		}

		static byte[] GetBytesNetworkOrder<T>(T In) {
			var obj = typeof(BitConverter);

			var ret = obj.GetMethod("GetBytes", new[] { typeof(T) }).Invoke(obj, new object[] { In });
			if (BitConverter.IsLittleEndian)
				return ((byte[])ret).Reverse().ToArray();
			return (byte[])ret;
		}

		static byte[] ReorderBytes(byte[] In) {
			return BitConverter.IsLittleEndian ? In.Reverse().ToArray() : In;
		}

		public async Task<CONNECTION_RETURN> InitConnection()
		{
			return await Connect(true);
		}

		async Task<CONNECTION_RETURN> Connect(bool WaitingBox, int ConnectionPoolIndex = -1)
		{
			var ini = Host.GetPluginSettings();
			var settings = ini.Sections["SDS-Relay"].Keys;

			string host = settings["Host"].Value;
			if (String.IsNullOrEmpty(host)) {
				if (InputBox.Launch("RT#-Relay", "Please enter RT#-Relay host", false, out host) != DialogResult.OK) {
					MessageBox.Show("Plugin disabled.", "RT#-Relay", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return CONNECTION_RETURN.PERMANENT_FAILURE;
				}

				settings["Host"].Value = host;
			}

			string sPort = settings["Port"].Value;
			ushort port;
			if (String.IsNullOrEmpty(sPort)) {
				if (InputBox.Launch("RT#-Relay", "Please enter RT#-Relay port", false, out sPort) != DialogResult.OK) {
					MessageBox.Show("Plugin disabled.", "RT#-Relay", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return CONNECTION_RETURN.PERMANENT_FAILURE;
				}

				settings["Port"].Value = sPort;
			}

			if (!UInt16.TryParse(sPort, out port)) {
				MessageBox.Show("Invalid port, plugin disabled.", "RT#-Relay", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return CONNECTION_RETURN.PERMANENT_FAILURE;
			}

			string pw = settings["Password"].Value;
			if (String.IsNullOrEmpty(pw)) {
				if (InputBox.Launch("RT#-Relay", "Please enter RT#-Relay connection password", true, out pw) != DialogResult.OK) {
					MessageBox.Show("Plugin disabled.", "RT#-Relay", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return CONNECTION_RETURN.PERMANENT_FAILURE;
				}

				settings["Password"].Value = pw;
			}

			WaitingBox wb = null;

			if (WaitingBox)
				wb = new WaitingBox("Connecting to " + host + ":" + sPort + "...", ProgressBarStyle.Marquee, ProgressBarStyle.Continuous);

			if (ConnectionPoolIndex == -1) {
				try {
					Connections.ForEach(x => x.Client.Close());
				} catch { }
				Connections.Clear();
			}

			if (WaitingBox)
				wb.Show();

			for (int x = ConnectionPoolIndex == -1 ? 0 : ConnectionPoolIndex;
				x < (ConnectionPoolIndex == -1 ? Int32.Parse(settings["Connections"].Value) : ConnectionPoolIndex + 1);
				x++) {
				var c = new Conn() {
					Lock = new SemaphoreSlim(1)
				};
				
				await c.Lock.WaitAsync();
				try {
					c.Client = new TcpClient();

					c.Client.ReceiveTimeout = Timeout;
					c.Client.SendTimeout = Timeout;

					try {
						await c.Client.ConnectAsync(host, port);
					} catch {
						if (WaitingBox)
							wb.Close();
						throw; // forward exception to plugin loader
					}

					if (!c.Client.Connected) {
						MessageBox.Show("Connection failed.", "RT#-Relay", MessageBoxButtons.OK, MessageBoxIcon.Error);
						return CONNECTION_RETURN.TEMPORARY_FAILURE;
					}

					var stm = c.Stream = c.Client.GetStream();
					c.Stream.ReadTimeout = Int32.Parse(settings["Timeout"].Value);

					if (WaitingBox)
						wb.UpdateText("Authenticating...");

					await stm.WriteAsync(Encoding.ASCII.GetBytes(pw), 0, pw.Length);

					var sw = new Stopwatch();
					sw.Start();

					await WriteInt(stm, (byte)SCGI_ACTION.VERSION);

					var remoteVersion = await ReadInt<ulong>(stm);

					sw.Stop();

					Ping = sw.ElapsedMilliseconds;

					if (remoteVersion < COMPATIBLE_REMOTE_VERSION.Item1 || remoteVersion > COMPATIBLE_REMOTE_VERSION.Item2) {
						MessageBox.Show("Incompatible relay server version (got v" + IntVersionToString(remoteVersion) +
							", expected between v" + IntVersionToString(COMPATIBLE_REMOTE_VERSION.Item1) + " and v" +
							IntVersionToString(COMPATIBLE_REMOTE_VERSION.Item2), "RT#-Relay", MessageBoxButtons.OK, MessageBoxIcon.Error);
						return CONNECTION_RETURN.PERMANENT_FAILURE;
					}

					/*if (response != 0x01) {
						MessageBox.Show("Invalid password.", "RT#-Relay", MessageBoxButtons.OK);
						settings["Password"].Value = "";
						return CONNECTION_RETURN.PERMANENT_FAILURE;
					}*/
				} finally {
					c.Lock.Release();
				}

				if (ConnectionPoolIndex == -1) {
#if DEBUG
					c.ConnectionID = x;
#endif
					Connections.Add(c);
				} else
					Connections[x] = c;
			}

			if (WaitingBox)
				wb.Close();
			Host.SavePluginSettings(ini);

			return CONNECTION_RETURN.OK;
		}

		private async Task<Tuple<TORRENT[], List<byte[]>>> GetTorrents(bool All) {
			var ret = new List<TORRENT>();

			var c = await GetAvailableConnection();
			var stm = c.Stream;
			uint removedLen;

			try {
				await WriteInt(stm, (byte)SCGI_ACTION.GET_ALL_TORRENT_ROWS);

				if (All)
					c.AllTorrentRowsTag = 0;

				await stm.WriteAsync(GetBytesNetworkOrder(c.AllTorrentRowsTag), 0, sizeof (uint));

				c.AllTorrentRowsTag = BitConverter.ToUInt32(await StmRead(stm, sizeof (uint)), 0);

				/*
				 * 
				 */
				Dictionary<int, string> trackerTable = null;
				trackerTable = new Dictionary<int, string>();

				var tableLen = await ReadInt<ushort>(stm);
				for (int x = 0; x < tableLen; x++)
					trackerTable.Add(x, await ReadString(stm));
				/*
				 * 
				 */

				// Is there actually anything to update?
				uint len = await ReadInt<uint>(stm);
				removedLen = await ReadInt<uint>(stm);

				for (int x = 0; x < len; x++) {
					int size;
					TORRENT cur = new TORRENT(this);

					size = await ReadInt<int>(stm);
					cur.Name = smaz.Decompress(await StmRead(stm, size, false));
					cur.State = (TORRENT_STATE)await ReadInt<ushort>(stm);
					cur.Size = await ReadInt<ulong>(stm);
					cur.Downloaded = await ReadInt<ulong>(stm);
					cur.Uploaded = await ReadInt<ulong>(stm);
					cur.DLSpeed = await ReadInt<uint>(stm);
					cur.UPSpeed = await ReadInt<uint>(stm);
					cur.CreatedOnDate = await ReadInt<ulong>(stm);
					cur.AddedOnDate = await ReadInt<ulong>(stm);
					cur.FinishedOnDate = await ReadInt<ulong>(stm);
					cur.Label = await ReadString(stm);
					var seedersConnected = await ReadInt<uint>(stm);
					var seedersTotal = await ReadInt<uint>(stm);
					cur.Seeders = new Tuple<uint, uint>(seedersConnected, seedersTotal);
					var peersConnected = await ReadInt<uint>(stm);
					var peersTotal = await ReadInt<uint>(stm);
					cur.Peers = new Tuple<uint, uint>(peersConnected, peersTotal);
					cur.Priority = (TORRENT_PRIORITY)await ReadInt<byte>(stm);
					cur.ChunkSize = await ReadInt<uint>(stm);
					
					var trkLen = await ReadInt<ushort>(stm);

					cur.Trackers = new List<TRACKER>();
					for (ushort i = 0;i < trkLen;i++) {
						var curTracker = new TRACKER(cur, i);
						curTracker.Uri = new Uri(trackerTable[await ReadInt<ushort>(stm)]);
						curTracker.Enabled = await ReadInt<bool>(stm);
						curTracker.Seeders = await ReadInt<uint>(stm);
						curTracker.Peers = await ReadInt<uint>(stm);
						curTracker.Downloaded = await ReadInt<uint>(stm);
						curTracker.LastUpdated = await ReadInt<ulong>(stm);
						curTracker.Interval = await ReadInt<uint>(stm);
						cur.Trackers.Add(curTracker);
					}

					cur.TrackerSingle = cur.Trackers
						.Where(t => t.Enabled && t.Uri.OriginalString != "dht://")
						.OrderByDescending(t => t.Seeders)
						.FirstOrDefault()?.Uri?.OriginalString;

					if (cur.TrackerSingle == null)
						cur.TrackerSingle = cur.Trackers.Count != 0 ? cur.Trackers[0].Uri.OriginalString : "N/A";

					cur.StatusMsg = await ReadString(stm);
					cur.Hash = await StmRead(stm, 20, false);

					/*
					 * 
					 */
					if (cur.UPSpeed > 1024 || cur.DLSpeed > 1024)
						cur.State |= TORRENT_STATE.ACTIVE;
					else
						cur.State |= TORRENT_STATE.INACTIVE;

					cur.Done = (float)cur.Downloaded / cur.Size * 100;

					cur.RemainingSize = cur.Size - cur.Downloaded;
					cur.Ratio = cur.Downloaded == 0 ? (cur.Uploaded == 0 ? 0 : Single.MaxValue) : (float)cur.Uploaded / cur.Downloaded;
					cur.ETA = (cur.Size - cur.Downloaded) == 0 ? 0 : cur.DLSpeed / (float)(cur.Size - cur.Downloaded);

					ret.Add(cur);
				}
			} finally {
				c.Lock.Release();
			}

			var RemovedTorrents = new List<byte[]>((int)removedLen);
			for (int x = 0; x < removedLen; x++)
				RemovedTorrents.Add(await StmRead(stm, 20, false));

			return Tuple.Create(ret.ToArray(), RemovedTorrents);
		}

		public async Task<ConnectionStatus> GetStatus()
		{
			await GetTorrentsLock.WaitAsync();
			var ret = new ConnectionStatus(UniqueGUID, "rtorrent", Connections.Count != 0 && Connections.All(x => x.Client.Connected)) {
				Description = "Ready (" + Connections.Sum(x => x.Client.Connected ? 1 : 0) + " / " + Connections.Count + ") (" + Ping + " ms)\n" +
				"↑ " + Utils.GetSizeSI(GlobalUpload) + "/s" + " // ↓ " + Utils.GetSizeSI(GlobalDownload) + "/s\n" +
				ActiveTorrents + " active torrent" + (ActiveTorrents == 1 ? "" : "s")
			};
			GetTorrentsLock.Release();
			return ret;
		}

		public SupportedOperations GetSupportedOperations()
		{
			return new SupportedOperations {
				files = {
					Download = true,
					GetDownloadStrategy = true,
					MediaInfo = true,
					SetDownloadStrategy = true,
					SetPriority = true,
					TransferTorrentDataToServer = true,
					GetRemoteDirectories = true,
					CreateRemoteDirectory = true
				},
				torrents = {
					EditTorrent = false,
					GetTorrentFile = true,
					SetPriority = true,
					FastResume = false
				},
				peers = {
					Ban = true,
					Kick = true,
					SnubUnsnub = true
				},
				trackers = {
					EnableDisable = true
				}
			};
		}

		public async Task SetServerSettings(ServerSetting[] In)
		{
			In = In.OrderBy(x => x.Id).ToArray();

			var c = await GetAvailableConnection();
			var stm = c.Stream;

			var ret = new List<ServerSetting>();

			try {
				await WriteInt(stm, (byte)SCGI_ACTION.SET_SETTINGS);

				for (int x = 0;x < In.Length;x++) {
					var type = In[x].CurrentValue;

					if (In[x].CurrentValue.GetType() == typeof(string))
						await WriteString(stm, In[x].CurrentValue);
					else
						await WriteInt(stm, In[x].CurrentValue);
				}

				// TODO: Handle (+implement) return values
				byte[] buff = new byte[1];
				await stm.ReadAsync(buff, 0, 1);
				Debug.Assert(buff[0] == 1);
				return;
			} finally {
				c.Lock.Release();
			}
		}

		public async Task<ServerSetting[]> GetServerSettings()
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;

			var ret = new List<ServerSetting>();

			try {
				await WriteInt(stm, (byte)SCGI_ACTION.GET_SETTINGS);

				string[] curCat = new[] { "Downloads" };

				for (int x = 0; x < 44; x++) {
					var settingId = x;
					var type = GetSettingType((await StmRead(stm, sizeof(byte)))[0]);

					dynamic value;
					if (type == typeof(string))
						value = await ReadString(stm);
					else
						value = await (dynamic)typeof(Main).GetMethod("ReadInt", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance).MakeGenericMethod(type).Invoke(null, new[] { stm }); // 👌

					string name = "SETTING ID " + settingId, desc = "N/A";
					dynamic defaultVal = "N/A";
					switch (settingId) {
						/* Downloads */
						case 0:
							name = "Number of upload slots";
							defaultVal = (long)100;
							break;
						case 1:
							name = "Minimum number of peers";
							defaultVal = (long)40;
							break;
						case 2:
							name = "Maximum number of peers";
							defaultVal = (long)100;
							break;
						case 3:
							name = "Minimum number of peers for seeding";
							defaultVal = (long)10;
							break;
						case 4:
							name = "Maximum number of peers for seeding";
							defaultVal = (long)100;
							break;
						case 5:
							name = "Wished number of peers";
							defaultVal = (long)-1;
							break;
						case 6:
							name = "Check hash after download";
							defaultVal = false;
							break;
						case 7:
							name = "Default directory for downloads";
							defaultVal = "/tmp";
							break;

						/* Connection */
						case 8:
							name = "Open listening port";
							defaultVal = true;
							curCat = new[] { "Connection", "Listening" };
							break;
							/* Listening port */
						case 9:
							name = "Port used for incoming connections";
							defaultVal = "10000-15000";
							break;
						case 10:
							name = "Randomize port";
							desc = "Randomize port each time rTorrent starts";
							defaultVal = true;
							break;
							/* Bandwidth Limiting */
						case 11:
							name = "Maximum upload rate";
							desc = "Global maximum upload rate (kB/s). 0: Unlimited";
							defaultVal = (long)0;
							curCat = new[] { "Connection", "Bandwidth limiting" };
							break;
						case 12:
							name = "Maximum download rate";
							desc = "Global maximum download rate (kB/s). 0: Unlimited";
							defaultVal = (long)0;
							break;
							/* Other limitations */
						case 13:
							name = "Global number of upload slots";
							defaultVal = (long)0;
							curCat = new[] { "Connection", "Other limitations" };
							break;
						case 14:
							name = "Global number of download slots";
							defaultVal = (long)0;
							break;
						case 15:
							name = "Maximum memory usage (MB)";
							defaultVal = (long)0;
							break;
						case 16:
							name = "Maximum number of open files";
							defaultVal = (long)128;
							break;
						case 17:
							name = "Maximum number of open http connections";
							defaultVal = (long)32;
							break;

						/* BitTorrent */
						case 18:
							name = "UDP port to use for DHT";
							desc = "Set to 0 to disable";
							defaultVal = (long)6881;
							curCat = new[] { "BitTorrent" };
							break;
						case 19:
							name = "Enable Peer Exchange (PEX)";
							defaultVal = true;
							break;
						case 20:
							name = "IP/Hostname to report to tracker";
							defaultVal = "0.0.0.0";
							break;

						/* Advanced */
						case 21:
							name = "http_cacert";
							defaultVal = "";
							curCat = new[] { "Advanced" };
							break;
						case 22:
							name = "http_capath";
							defaultVal = "";
							break;
						case 23:
							name = "max_downloads_div";
							defaultVal = (long)1;
							break;
						case 24:
							name = "max_uploads_div";
							defaultVal = (long)1;
							break;
						case 25:
							name = "max_file_size";
							defaultVal = (long)137438953472;
							break;
						case 26:
							name = "preload_type";
							defaultVal = new KeyValuePair<string, bool>[3] {
								new KeyValuePair<string, bool>("off", true),
								new KeyValuePair<string, bool>("madvise", false),
								new KeyValuePair<string, bool>("direct paging", false)
							};
							break;
						case 27:
							name = "preload_min_size";
							defaultVal = (long)262144;
							break;
						case 28:
							name = "preload_required_rate";
							defaultVal = (long)5120;
							break;
						case 29:
							name = "receive_buffer_size";
							defaultVal = (long)0;
							break;
						case 30:
							name = "send_buffer_size";
							defaultVal = (long)0;
							break;
						case 31:
							name = "safe_sync";
							defaultVal = false;
							break;
						case 32:
							name = "timeout_safe_sync";
							defaultVal = (long)900;
							break;
						case 33:
							name = "timeout_sync";
							defaultVal = (long)600;
							break;
						case 34:
							name = "scgi_dont_route";
							defaultVal = false;
							break;
						case 35:
							name = "session";
							defaultVal = "/home/user/.config/rtorrent/session/";
							break;
						case 36:
							name = "session_lock";
							defaultVal = true;
							break;
						case 37:
							name = "session_on_completion";
							defaultVal = true;
							break;
						case 38:
							name = "split_file_size";
							defaultVal = (long)-1;
							break;
						case 39:
							name = "split_suffix";
							defaultVal = ".part";
							break;
						case 40:
							name = "use_udp_trackers";
							defaultVal = true;
							break;
						case 41:
							name = "http_proxy";
							defaultVal = "";
							break;
						case 42:
							name = "proxy_address";
							defaultVal = "0.0.0.0";
							break;
						case 43:
							name = "bind";
							defaultVal = "0.0.0.0";
							break;
					}

					ret.Add(new ServerSetting((uint)settingId, name, desc, curCat, defaultVal, value));
				}
			} finally {
				c.Lock.Release();
			}

			return ret.ToArray();
		}

		Type GetSettingType(byte recv)
		{
			switch (recv) {
				case (int)TypeCode.Boolean:
					return typeof(bool);
				case (int)TypeCode.Byte:
					return typeof(byte);
				case (int)TypeCode.Char:
					return typeof(char);
				case (int)TypeCode.Decimal:
					return typeof(decimal);
				case (int)TypeCode.Double:
					return typeof(double);
				case (int)TypeCode.Int16:
					return typeof(short);
				case (int)TypeCode.Int32:
					return typeof(int);
				case (int)TypeCode.Int64:
					return typeof(long);
				case (int)TypeCode.Single:
					return typeof(float);
				case (int)TypeCode.String:
					return typeof(string);
				case (int)TypeCode.UInt16:
					return typeof(ushort);
				case (int)TypeCode.UInt32:
					return typeof(uint);
				case (int)TypeCode.UInt64:
					return typeof(ulong);
				default:
					throw new ArgumentOutOfRangeException(nameof(recv));
			}
		}

		public async Task<CONNECTION_RETURN> Reconnect()
		{
			return await Connect(false);
		}

		public async Task<TORRENT[]> GetAll() {
			var ret = await GetTorrents(true);
			return ret.Item1;
		}

		public async Task<Tuple<TORRENT[], byte[][]>> GetAllChanged() {
			var ret = await GetTorrents(false);
			return Tuple.Create(ret.Item1, ret.Item2.ToArray());
		}

		public async Task LoadRaw(byte[] Torrent, bool StartOnAdd, string RemotePath) {
			/*if (FastResume)
				Debug.Assert(RemotePath != null);

			if (FastResume) {
				// Rebuild torrent with libtorrent fastresume data

				var t = new TorrentManip(Torrent);
				var files = new List<string>();
				var libtorrentResume = new BencodeNET.Objects.BDictionary();

				if (t.Torrent.Files != null) {
					foreach (var file in t.Torrent.Files)
						files.Add(PathCombine(RemotePath, file.FullPath));
				} else
					files.Add(PathCombine(RemotePath, t.Torrent.File.FileName));

				libtorrentResume.Add("bitfield", (t.Torrent.TotalSize + t.Torrent.PieceSize - 1) / t.Torrent.PieceSize);

				var mtimes = await TorrentActionEx(SCGI_ACTION.I_FR_GET_MTIMES, null, true, files.Select(x => new[] { x }), async (stm) => {
					var ret = new List<long>();

					for (int x = 0;x < files.Count;x++)
						ret.Add(await ReadInt<long>(stm));
					
					return ret;
				});

				// No files
				if (mtimes.Max() != 0) {
					var resumeFiles = new BencodeNET.Objects.BList();

					for (int x = 0;x < mtimes.Count;x++) {
						var f = new BencodeNET.Objects.BDictionary();

						f.Add("mtime", (long)mtimes[x]);
						f.Add("priority", mtimes[x] != 0 ? 0 : 2);
						f.Add("competed", Math.Ceiling((mtimes.Count == 1 ? t.Torrent.File.FileSize : t.Torrent.Files[x].FileSize) / (double)t.Torrent.PieceSize).ToString());
						resumeFiles.Add(f);
					}

					libtorrentResume.Add("files", resumeFiles);

					t.Torrent.ExtraFields.Add("libtorrent_resume", libtorrentResume);
					Torrent = t.Torrent.EncodeAsBytes();
				}
			}*/

			await TorrentActionEx<object>(SCGI_ACTION.LOAD_TORRENT_RAW, new dynamic[] {
				StartOnAdd,
				RemotePath
			}, false, null, async (stm) => {
				await WriteInt(stm, Torrent.Length);

				for (int x = 0; x < Math.Ceiling((double)Torrent.Length / BUFFER_SIZE); x++) {
					int blockSize = (Torrent.Length - x * BUFFER_SIZE < BUFFER_SIZE) ? Torrent.Length - x * BUFFER_SIZE : BUFFER_SIZE;
					byte[] block = new byte[blockSize];
					Buffer.BlockCopy(Torrent, x * BUFFER_SIZE, block, 0, blockSize);
					await stm.WriteAsync(block, 0, blockSize);
				}

				return null;
			});
		}

		public async Task LoadUri(Uri Uri, bool StartOnAdd, string RemotePath) {
			if (Uri.Scheme == "http" || Uri.Scheme == "https") {
				var wc = new WebClient();
				var data = wc.DownloadData(Uri);

				new TorrentManip(data);

				await LoadRaw(data, StartOnAdd, RemotePath);
			} else if (Uri.Scheme == "magnet") {
				await TorrentAction(SCGI_ACTION.LOAD_TORRENT_STRING, new dynamic[] {
					StartOnAdd,
					RemotePath,
					Uri.OriginalString
				});
			} else
				throw new ArgumentException("Unsupported URI protocol");
		}

		private async Task StmWriteDynamic(NetworkStream stm, dynamic p)
		{
			if (p is byte[])
				await stm.WriteAsync(p, 0, p.Length);
			else if (p is string)
				await WriteString(stm, p);
			else
				await WriteInt(stm, p);
		}

		private async Task TorrentAction(SCGI_ACTION Action, dynamic[] Params)
		{
			await TorrentActionEx(Action, Params, false, null);
		}

		private async Task TorrentAction(SCGI_ACTION Action, TORRENT[] Torrent)
		{
			await TorrentActionEx(Action, new dynamic[] { }, true, Torrent.Select(x => new dynamic[] { x.Hash }).ToArray());
		}

		private async Task TorrentAction(SCGI_ACTION Action, dynamic[] ParamsPre, TORRENT[] Torrents)
		{
			await TorrentActionEx(Action, ParamsPre, true, Torrents.Select(x => new dynamic[] { x.Hash }).ToArray());
		}

		private async Task<T> TorrentActionEx<T>(
			SCGI_ACTION Action,
			IEnumerable<dynamic> ParamsPre,
			bool SendCount,
			IEnumerable<IEnumerable<dynamic>> ParamsLoop,
			Func<NetworkStream, Task<T>> PostFx)
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			try {
				await WriteInt(stm, (byte)Action);

				if (ParamsPre != null) {
					foreach (var p in ParamsPre)
						await StmWriteDynamic(stm, p);
				}

				if (ParamsLoop != null) {
					if (SendCount)
						await WriteInt(stm, ParamsLoop.Count());

					foreach (var x in ParamsLoop) {
						foreach (var p in x)
							await StmWriteDynamic(stm, p);
					}
				}

				return await PostFx(stm);
			} finally {
				c.Lock.Release();
			}
		}

		private async Task TorrentActionEx(SCGI_ACTION Action, dynamic[] ParamsPre, bool SendCount, dynamic[][] ParamsLoop)
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			try {
				await WriteInt(stm, (byte)Action);

				foreach (var p in ParamsPre)
					await StmWriteDynamic(stm, p);

				if (ParamsLoop != null) {
					if (SendCount)
						await WriteInt(stm, ParamsLoop.Count());

					foreach (var x in ParamsLoop) {
						foreach (var p in x)
							await StmWriteDynamic(stm, p);
					}
				}
			} finally {
				c.Lock.Release();
			}
		}

		public async Task Start(TORRENT[] Torrent) {
			await TorrentAction(SCGI_ACTION.START_TORRENTS_MULTI, Torrent);
		}

		public async Task Pause(TORRENT[] Torrent) {
			await TorrentAction(SCGI_ACTION.PAUSE_TORRENTS_MULTI, Torrent);
		}

		public async Task Stop(TORRENT[] Torrent) {
			await TorrentAction(SCGI_ACTION.STOP_TORRENTS_MULTI, Torrent);
		}

		public async Task ForceRecheck(TORRENT[] Torrent) {
			await TorrentAction(SCGI_ACTION.FORCE_RECHECK_TORRENTS_MULTI, Torrent);
		}

		public async Task Reannounce(TORRENT[] Torrent) {
			await TorrentAction(SCGI_ACTION.REANNOUNCE_TORRENTS_MULTI, Torrent);
		}

		public async Task AddPeer(TORRENT Torrent, IPAddress PeerIP, ushort PeerPort) {
			await TorrentAction(SCGI_ACTION.ADD_PEER_TORRENT, new dynamic[] {
				Torrent.Hash,
				(PeerIP.AddressFamily == AddressFamily.InterNetworkV6 ? "[" + PeerIP + "]" : PeerIP.ToString()) + ":" + PeerPort
			});
		}

		public async Task SetLabel(TORRENT[] Torrent, string Label) {
			await TorrentAction(SCGI_ACTION.SET_LABEL_TORRENTS_MULTI, new dynamic[] { Label }, Torrent);
		}

		public async Task SetPriority(TORRENT[] Torrent, TORRENT_PRIORITY Priority) {
			await TorrentAction(SCGI_ACTION.SET_PRIORITY_TORRENTS_MULTI, new dynamic[] { (byte)Priority }, Torrent);
		}

		public async Task Remove(TORRENT[] Torrent) {
			await TorrentAction(SCGI_ACTION.REMOVE_TORRENTS_MULTI, Torrent);
		}

		public async Task RemoveTorrentAndData(TORRENT[] Torrent) {
			await TorrentAction(SCGI_ACTION.REMOVE_TORRENTS_PLUS_DATA_MULTI, Torrent);
		}

		public async Task<byte[][]> GetTorrentFiles(TORRENT[] Torrent) {
			return await TorrentActionEx(SCGI_ACTION.GET_TORRENT_FILES_MULTI,
				new dynamic[] { },
				true,
				Torrent.Select(x => new dynamic[] { x.Hash }).ToArray(),
				async (stm) => {
					var retCount = await ReadInt<uint>(stm);

					var ret = new byte[retCount][];
					for (int x = 0; x < retCount; x++) {
						var torrentSize = await ReadInt<uint>(stm);
						if (torrentSize == 0)
							ret[x] = null;
						else
								ret[x] = await StmRead(stm, (int)torrentSize, false);
					}

					return ret;
			});
		}

		public async Task<PEER[]> GetPeers(TORRENT Torrent) {
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			List<PEER> ret;

			try {
				await WriteInt(stm, (byte)SCGI_ACTION.GET_TORRENT_PEERS);
				await stm.WriteAsync(Torrent.Hash, 0, Torrent.Hash.Length);

				// Are there actually any peers?
				uint len = await ReadInt<uint>(stm);

				ret = new List<PEER>();

				for (int x = 0; x < len; x++) {
					var cur = new PEER(Torrent, await StmRead(stm, 20, false));

					byte ipv = await ReadInt<byte>(stm);
					cur.IP = new IPAddress(await StmRead(stm, ipv == 4 ? 4 : 16, false));
					cur.Port = await ReadInt<ushort>(stm);
					cur.Client = await ReadString(stm);
					cur.flags = (PEER_FLAGS)(await ReadInt<byte>(stm));
					cur.downloaded = await ReadInt<ulong>(stm);
					cur.uploaded = await ReadInt<ulong>(stm);
					cur.dlSpeed = await ReadInt<uint>(stm);
					cur.upSpeed = await ReadInt<uint>(stm);
					ret.Add(cur);
				}
			} finally {
				c.Lock.Release();
			}
			return ret.ToArray();
		}

		public async Task<FILES[]> GetFiles(TORRENT Torrent)
		{
			var c = await GetAvailableConnection();
			var stm = c.Stream;
			Dictionary<string, FILE> filesRaw = new Dictionary<string, FILE>();

			try {
				await WriteInt(stm, (byte)SCGI_ACTION.GET_TORRENT_FILES);
				await stm.WriteAsync(Torrent.Hash, 0, Torrent.Hash.Length);

				var count = await ReadInt<uint>(stm);

				for (int i = 0; i < count; i++) {
					var size = await ReadInt<ushort>(stm);
					var path = smaz.Decompress(await StmRead(stm, size, false));
					var curFile = new FILE(Torrent, UniqueGUID, new FILE_ID(i, path));
					curFile.size = await ReadInt<ulong>(stm);
					curFile.downloadedChunks = await ReadInt<ulong>(stm);
					curFile.priority = (FILE.PRIORITY)await ReadInt<byte>(stm);
					curFile.downloadStrategy = (FILE.DOWNLOAD_STRATEGY)await ReadInt<byte>(stm);

					filesRaw.Add(path, curFile);
				}
			} finally {
				c.Lock.Release();
			}
			List<FILES> files = new List<FILES>();

			foreach (var el in filesRaw) {
				var dirs = el.Key.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

				FILE curFile = el.Value;
				if (dirs.Length == 1) {
					curFile.name = dirs[0];
					files.Add(new FILES(-1, curFile, -1));
					files[files.Count - 1].ArrayIndex = files.Count - 1;
					continue;
				}

				Func<int, List<int>, string[], int, int> findParent = null;
				findParent = (root, children, inDirs, startDir) => {
					foreach (int ch in children) {
						if (files[ch].Item.name == inDirs[startDir] && files[ch].Item.Directory) {
							if (files[ch].Children.Count == 1 && !files[files[ch].Children[0]].Item.Directory)
								return ch;
							return findParent(ch, files[ch].Children, inDirs, startDir + 1);
						}
					}
					return root;
				};

				// Get all root folders
				var indexes = new List<int>();
				for (int x = 0; x < files.Count; x++) {
					if (files[x].Parent == -1 && files[x].Item.Directory) {
						indexes.Add(x);
					}
				}

				var parent = findParent(-1, indexes, dirs, 0);

				curFile.name = dirs[dirs.Length - 1];

				if (parent == -1 || files[parent].Item.name != dirs[dirs.Length - 2]) {
					// If we are missing folders

					var xs = 0;
					if (parent != -1) {
						// Find missing folders start
						for (int x = dirs.Length - 1; x >= 0; x--) {
							if (files[parent].Item.name == dirs[x]) {
								xs = x + 1;
								break;
							}
						}
					}

					for (int x = xs; x < dirs.Length - 1; x++) {
						if (x == 0) {
							// Add folder to root
							var folder = new FILE(dirs[x], Torrent, Torrent.Owner.UniqueGUID, -1) {
								priority = FILE.PRIORITY.NA,
								downloadStrategy = FILE.DOWNLOAD_STRATEGY.NA
							};
							files.Add(new FILES(-1, folder, -1));
							files[files.Count - 1].ArrayIndex = files.Count - 1;
						} else {
							// Add other missing folders
							var folder = new FILE(dirs[x], Torrent, Torrent.Owner.UniqueGUID, -1) {
								priority = FILE.PRIORITY.NA,
								downloadStrategy = FILE.DOWNLOAD_STRATEGY.NA
							};
							files.Add(new FILES(parent, folder, -1));
							files[files.Count - 1].ArrayIndex = files.Count - 1;
							files[parent].Children.Add(files.Count - 1);
						}
						parent = files.Count - 1;
					}

					// Add the file
					files.Add(new FILES(files.Count - 1, curFile, -1));
					files[files.Count - 1].ArrayIndex = files.Count - 1;
					files[files.Count - 2].Children.Add(files.Count - 1);
				} else {
					// Else, add file at top of existing folders
					files.Add(new FILES(parent, curFile, -1));
					files[files.Count - 1].ArrayIndex = files.Count - 1;
				}
			}

			// Add everything on top or the root folder
			if (files.Count > 1) {
				var folder = new FILE(Torrent.Name, Torrent, Torrent.Owner.UniqueGUID, -1) {
					priority = FILE.PRIORITY.NA,
					downloadStrategy = FILE.DOWNLOAD_STRATEGY.NA
				};
				files.Add(new FILES(-1, folder, -1));
				files[files.Count - 1].ArrayIndex = files.Count - 1;
				var i = files.Count - 1;
				for (int x = 0; x < files.Count - 1; x++) {
					if (files[x].Parent == -1) {
						files[x].Parent = i;
						files[i].Children.Add(x);
					}
				}
			}

			return files.ToArray();
		}

		async Task UploadFile(string Path, NetworkStream stm, string Destination, WaitingBox wbox)
		{
			var fs = new FileStream(Path, FileMode.Open, FileAccess.Read);

			await WriteInt(stm, (byte)SCGI_ACTION.TRANSFER_FILE);
			await WriteInt(stm, fs.Length);
			await WriteString(stm, Destination);

			if (!await ReadInt<bool>(stm)) {
				fs.Dispose();
				throw new IOException("Failed to open remote file: " + await ReadString(stm));
			}

			var transferCaps = new Queue<Tuple<TimeSpan, ulong>>();
			var sw = new Stopwatch();
			sw.Start();
			var sSize = GetSizeSI(fs.Length);

			byte[] buff = new byte[4096];
			var blocks = Math.Floor((decimal)fs.Length / 4096);
			ulong secondBytes = 0;
			for (ulong x = 0; x < blocks; x++) {
				await fs.ReadAsync(buff, 0, 4096);
				await stm.WriteAsync(buff, 0, 4096);
				secondBytes += 4096;

				if (sw.ElapsedMilliseconds > 1000) {
					if (transferCaps.Count > 3)
						transferCaps.Dequeue();

					var bytes = transferCaps.Sum(t => (float)t.Item2);
					var seconds = transferCaps.Sum(t => (float)t.Item1.TotalMilliseconds);
					sw.Restart();

					wbox.UpdateText(Destination + "\n" + GetSizeSI((ulong)(bytes / seconds)) + "/s\n" +
						GetSizeSI(fs.Position) + " / " + sSize);
					Application.DoEvents();
					transferCaps.Enqueue(Tuple.Create(sw.Elapsed, secondBytes));
					secondBytes = 0;
				}

				wbox.UpdatePBarSingle((byte)Math.Floor(x / blocks * 100));
			}

			await fs.ReadAsync(buff, 0, (int)(fs.Length % 4096));
			await stm.WriteAsync(buff, 0, (int)(fs.Length % 4096));

			wbox.UpdatePBarSingle(100);

			fs.Dispose();
		}

		public async Task TransferTorrentDataToServer(string Path, string Destination)
		{
			FileStream fs;

			var c = await GetAvailableConnection();
			var stm = c.Stream;

			try {
				string res = "No such path";
				bool ret = false;

				var wbox = new WaitingBox("Transfering data...", ProgressBarStyle.Blocks, ProgressBarStyle.Blocks);
				wbox.Show();

				if (File.Exists(Path))
					await UploadFile(Path, stm, System.IO.Path.Combine(Destination, System.IO.Path.GetFileName(Path)).Replace('\\', '/'), wbox);
				else if (Directory.Exists(Path)) {
					var files = GetFilesWildcard(System.IO.Path.Combine(Path, "*"));
					for (int x = 0;x < files.Length;x++) {
						await UploadFile(files[x], stm, System.IO.Path.Combine(Destination, new DirectoryInfo(System.IO.Path.GetDirectoryName(files[x])).Name, System.IO.Path.GetFileName(files[x])).Replace('\\', '/'), wbox);

						wbox.UpdatePBarAll((byte)((float)x / files.Length * 100));
					}
				}

				wbox.Close();
			} finally {
				c.Lock.Release();
			}
		}

		public async Task<string> GetDefaultSavePath()
		{
			return (await GetServerSettings()).First(x => x.Name == "Default directory for downloads").CurrentValue;
		}

		public async Task ToggleTracker(TRACKER Tracker, bool Enable)
		{
			await TorrentAction(SCGI_ACTION.TOGGLE_TRACKER, new dynamic[] {
				Tracker.Parent.Hash,
				(ushort)Tracker.ID,
				Enable
			});
		}

		public async Task KickPeer(PEER Peer)
		{
			await TorrentAction(SCGI_ACTION.KICK_PEER, new dynamic[] {
				Peer.Parent.Hash,
				(byte[])Peer.ID
			});
		}

		public async Task BanPeer(PEER Peer)
		{
			await TorrentAction(SCGI_ACTION.BAN_PEER, new dynamic[] {
				Peer.Parent.Hash,
				(byte[])Peer.ID
			});
		}

		public async Task TogglePeerSnub(PEER Peer, bool Snub)
		{
			await TorrentAction(SCGI_ACTION.TOGGLE_SNUB_PEER, new dynamic[] {
				Peer.Parent.Hash,
				(byte[])Peer.ID,
				Snub
			});
		}

		public async Task SetFilePriority(FILE File, FILE.PRIORITY In)
		{
			await TorrentAction(SCGI_ACTION.SET_FILE_PRIORITY, new dynamic[] {
				File.Parent.Hash,
				((FILE_ID)File.ID).id,
				(byte)In
			});
		}

		public async Task SetFileDownloadStrategy(FILE File, FILE.DOWNLOAD_STRATEGY In)
		{
			await TorrentAction(SCGI_ACTION.SET_FILE_DOWNLOAD_STRATEGY, new dynamic[] {
				File.Parent.Hash,
				((FILE_ID)File.ID).id,
				(byte)In
			});
		}

		public async Task FileMediaInfo(FILE File)
		{
			Func<NetworkStream, Task<object>> Fx = async (stm) => {
				var ret = await ReadInt<bool>(stm);
				var str = (await ReadString(stm)).Replace("\n", Environment.NewLine);

				if (!ret)
					throw new Exception("Failed to MediaInfo:" + Environment.NewLine + str);

				return null;
			};

			var IsSingleFile = File.Parent.FileList.Count == 1;

			if (IsSingleFile)
				await TorrentActionEx(SCGI_ACTION.FILE_MEDIAINFO, new dynamic[] {
					File.Parent.Hash,
					IsSingleFile
				}, false, null, Fx);
			else
				await TorrentActionEx(SCGI_ACTION.FILE_MEDIAINFO, new dynamic[] {
					File.Parent.Hash,
					IsSingleFile,
					((FILE_ID)File.ID).fullPath
				}, false, null, Fx);
		}

		public async Task DownloadFile(FILE File, string LocalPath)
		{
			Func<NetworkStream, Task<object>> Fx = async (stm) => {
				var status = await ReadInt<bool>(stm);

				if (!status)
					throw new Exception("Failed to download file:" + Environment.NewLine + await ReadString(stm));

				var fs = new FileStream(LocalPath, FileMode.Create);

				var wb = new WaitingBox("Downloading...", ProgressBarStyle.Marquee, ProgressBarStyle.Blocks);
				wb.Show();

				var size = await ReadInt<ulong>(stm);
				var origSize = size;
				
				var blocks = Math.Ceiling((decimal)size / 4096);
				var buffer = new byte[4096];
				ulong bytes;
				while (size != 0) {
					bytes = (ulong)(await stm.ReadAsync(buffer, 0, 4096));
					await fs.WriteAsync(buffer, 0, (int)bytes);
					size -= bytes;

					wb.UpdatePBarAll((byte)(size / origSize * 100));
				}

				wb.Close();
				fs.Dispose();

				return null;
			};

			var IsSingleFile = File.Parent.FileList.Count == 1;

			if (IsSingleFile)
				await TorrentActionEx(SCGI_ACTION.DOWNLOAD_FILE, new dynamic[] {
					File.Parent.Hash,
					IsSingleFile
				}, false, null, Fx);
			else
				await TorrentActionEx(SCGI_ACTION.DOWNLOAD_FILE, new dynamic[] {
					File.Parent.Hash,
					IsSingleFile,
					((FILE_ID)File.ID).fullPath
				}, false, null, Fx);
		}

		public async Task EditTorrent(TORRENT Torrent, string[] SetTrackers, string Comment)
		{
			var bytes = (await GetTorrentFiles(new[] { Torrent }))[0];
			var torrentManip = new TorrentManip(bytes);

			torrentManip.SetTrackers(SetTrackers);
			torrentManip.Torrent.Comment = Comment;
			var raw = torrentManip.Torrent.EncodeAsBytes();

			await Remove(new[] { Torrent });
			await LoadRaw(raw, true, Torrent.RemotePath);
		}

		public async Task<string[]> GetRemoteDirectories(string Path)
		{
			return await TorrentActionEx(SCGI_ACTION.LIST_DIRECTORIES, new dynamic[] {
				Path
			}, false, null, async (stm) => {
				List<string> ret = new List<string>();

				var ok = await ReadInt<bool>(stm);

				if (!ok)
					throw new Exception(await ReadString(stm));

				var count = await ReadInt<uint>(stm);
				for (uint x = 0;x < count;x++)
					ret.Add(await ReadString(stm));

				return ret.ToArray();
			});
		}

		public async Task CreateRemoteDirectory(string Path)
		{
			await TorrentAction(SCGI_ACTION.CREATE_DIRECTORY, new[] { Path });
		}
	}
}