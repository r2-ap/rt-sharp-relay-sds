﻿namespace RTSharp_Relay_SDS
{
	partial class CustomCommand
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomCommand));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.t_command = new System.Windows.Forms.TextBox();
			this.b_close = new System.Windows.Forms.Button();
			this.t_result = new System.Windows.Forms.TextBox();
			this.b_exec = new System.Windows.Forms.Button();
			this.b_save = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Controls.Add(this.t_command);
			this.groupBox1.Controls.Add(this.b_exec);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1104, 463);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Execute custom XML";
			// 
			// t_command
			// 
			this.t_command.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.t_command.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.t_command.Location = new System.Drawing.Point(6, 20);
			this.t_command.Multiline = true;
			this.t_command.Name = "t_command";
			this.t_command.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.t_command.Size = new System.Drawing.Size(1005, 54);
			this.t_command.TabIndex = 0;
			this.t_command.Text = resources.GetString("t_command.Text");
			// 
			// b_close
			// 
			this.b_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_close.Location = new System.Drawing.Point(1017, 428);
			this.b_close.Name = "b_close";
			this.b_close.Size = new System.Drawing.Size(75, 23);
			this.b_close.TabIndex = 1;
			this.b_close.Text = "Close";
			this.b_close.UseVisualStyleBackColor = true;
			this.b_close.Click += new System.EventHandler(this.b_close_Click);
			// 
			// t_result
			// 
			this.t_result.Dock = System.Windows.Forms.DockStyle.Fill;
			this.t_result.Location = new System.Drawing.Point(3, 16);
			this.t_result.Multiline = true;
			this.t_result.Name = "t_result";
			this.t_result.ReadOnly = true;
			this.t_result.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.t_result.Size = new System.Drawing.Size(1080, 323);
			this.t_result.TabIndex = 2;
			// 
			// b_exec
			// 
			this.b_exec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.b_exec.Location = new System.Drawing.Point(1017, 51);
			this.b_exec.Name = "b_exec";
			this.b_exec.Size = new System.Drawing.Size(75, 23);
			this.b_exec.TabIndex = 1;
			this.b_exec.Text = "Execute";
			this.b_exec.UseVisualStyleBackColor = true;
			this.b_exec.Click += new System.EventHandler(this.b_exec_Click);
			// 
			// b_save
			// 
			this.b_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.b_save.Location = new System.Drawing.Point(936, 428);
			this.b_save.Name = "b_save";
			this.b_save.Size = new System.Drawing.Size(75, 23);
			this.b_save.TabIndex = 2;
			this.b_save.Text = "Save";
			this.b_save.UseVisualStyleBackColor = true;
			this.b_save.Click += new System.EventHandler(this.b_save_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.t_result);
			this.groupBox2.Location = new System.Drawing.Point(6, 80);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(1086, 342);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Result";
			// 
			// PluginSettings
			// 
			this.AcceptButton = this.b_save;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1104, 463);
			this.Controls.Add(this.b_save);
			this.Controls.Add(this.b_close);
			this.Controls.Add(this.groupBox1);
			this.Name = "PluginSettings";
			this.ShowIcon = false;
			this.Text = "RTSharp-Relay-SDS";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox t_result;
		private System.Windows.Forms.TextBox t_command;
		private System.Windows.Forms.Button b_exec;
		private System.Windows.Forms.Button b_close;
		private System.Windows.Forms.Button b_save;
	}
}